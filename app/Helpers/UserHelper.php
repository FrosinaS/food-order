<?php
/**
 * Created by PhpStorm.
 * User: Frose
 * Date: 30.07.2015
 * Time: 00:07
 */

namespace App\Helpers;


class UserHelper
{

    public static function isAdmin()
    {
        $user = \Auth::user();
        return ($user != null && $user->role == 'admin') ? true : false;
    }
}