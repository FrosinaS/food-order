<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $table = "orders";

    protected $fillable=['user_id',
                         'item_id',
                         'date',
                         'termin_id',
                         'created_at',
                         'updated_at'];


    public function time()
    {
        return $this->belongsTo('App\Models\Time', 'termin_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }

}
