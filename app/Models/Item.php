<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model {


    protected $table = "items";
    protected $fillable = [ 'name',
                            'description',
                            'category_id',
                            'price',
                            'weight',
                            'status',
                            'monday',
                            'tuesday',
                            'wednesday',
                            'thursday',
                            'friday',
                            'vegetarian',
                            'created_at',
                            'updated_at'];


    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
}
