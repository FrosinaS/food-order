<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Items extends Model {


    protected $table = "items";

    use SoftDeletes;

    protected $fillable = ['name', 'description', 'category_id', 'price', 'weight', 'status', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'created_at', 'updated_at'];
    protected $guarded = ['id'];

    public function getAll() {

        $items = DB::table('items')
                ->leftJoin('categories', 'items.category_id', '=', 'categories.id')->orderBy('items.created_at', 'desc')
                ->select('items.*', 'categories.category_name')
                ->get();
        return $items;
    }

    public function getAllFilter($where, $order_by, $order_type, $like_column, $like_term) {

        $items = DB::table('items')
                ->leftJoin('categories', 'items.category_id', '=', 'categories.id')
                ->select('items.*', 'categories.category_name');
        if ($where) {
            $items->where($where);
        }

        if ($order_by) {
            if ($order_type) {
                $items->orderBy($order_by, $order_type);
            } else {
                $items->orderBy($order_by);
            }
        } else
            $items->orderBy('items.created_at', 'desc');
        if ($like_column)
            $items->like($like_column, $like_term);

        $it = $items->get();
        return $it;
    }


    public function orders()
    {
        return $this->hasMany('App\Models\Orders', 'id', 'item_id');
    }

    public function saveItem($input) {

        $item = new Items;

        $item->name = $input['name'];
        $item->description = $input['description'];
        $item->category_id = $input['category'];
        $item->price = $input['price'];
        $item->weight = $input['weight']; // if isset
        $item->status = $input['status'];
        $item->monday = (isset($input['monday'])) ? $input['monday'] : '0';
        $item->tuesday = (isset($input['tuesday'])) ? $input['tuesday'] : '0';
        $item->wednesday = (isset($input['wednesday'])) ? $input['wednesday'] : '0';
        $item->thursday = (isset($input['thursday'])) ? $input['thursday'] : '0';
        $item->friday = (isset($input['friday'])) ? $input['friday'] : '0';

        $item->save();
    }

    public function getItem($id) {

        $item = Items::find($id);
        return $item;
    }

    public function updateItem($id, $input) {

        $item = Items::find($id);

        $item->name = $input['name'];
        $item->description = $input['description'];
        $item->category_id = $input['category'];
        $item->price = $input['price'];
        $item->weight = $input['weight']; // if isset
        $item->status = $input['status'];
        $item->monday = (isset($input['monday'])) ? $input['monday'] : '0';
        $item->tuesday = (isset($input['tuesday'])) ? $input['tuesday'] : '0';
        $item->wednesday = (isset($input['wednesday'])) ? $input['wednesday'] : '0';
        $item->thursday = (isset($input['thursday'])) ? $input['thursday'] : '0';
        $item->friday = (isset($input['friday'])) ? $input['friday'] : '0';

        $item->save();
    }

    public function deleteItem($id) {

        $item = Items::find($id);

        $item->delete();
    }

    public function setStatus($id, $status) {
        $item = Items::find($id);
        $item->status = $status;
        $item->save();
    }

}
