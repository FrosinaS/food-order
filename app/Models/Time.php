<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table="times";

    protected $fillable=['time',
                         'max_people',
                         'status',
                         'created_at',
                         'updated_at'];

    public function orders()
    {
        return $this->hasMany('App\Models\Orders', 'id', 'termin_id');
    }
}
