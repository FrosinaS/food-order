<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

    protected $table="categories";

    protected $fillable = [ 'id',
                            'name',
                            'description',
                            'status',
                            'created_at',
                            'updated_at'];

    public function items()
    {
        return $this->hasMany('App\Models\Item', 'category_id', 'id');
    }

}
