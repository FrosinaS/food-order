<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddNewTimeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        if($id != null) {
            return [
                'time' => 'required|unique:times,time,' . $id,
                'max-people' => 'required|integer'
            ];
        }
        else
        {
            return [
                'time' => 'required|unique:times,time',
                'max-people' => 'required|integer'
            ];
        }
    }
}
