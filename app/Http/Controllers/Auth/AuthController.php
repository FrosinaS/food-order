<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use \Auth;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest',
                           ['except' => ['getLogout',
                            'getEdit',
                            'postEdit',
                            'getEditMyProfile',
                             'getRegister',
                             'postRegister']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255\unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'first-name' => 'min:2|max:255',
            'last-name' => 'min:2|max:255'
        ]);
    }

    protected function updateValidator(array $data, $id)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users,username,'.$id,
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            'first-name' => 'min:2|max:255',
            'last-name' => 'min:2|max:255'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'first_name' => $data['first-name'],
            'last_name' => $data['last-name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role' => 'user',
            'status' => 0,
            'vegetarian' => ((isset($data['vegetarian']) == true && $data['vegetarian'] == 'on') ? 1 : 0)
        ]);
    }

    protected function update(array $data, $id)
    {
        return User::where('id', $id)->update([
            'username' => $data['username'],
            'first_name' => $data['first-name'],
            'last_name' => $data['last-name'],
            'email' => $data['email'],
            'vegetarian' => ((isset($data['vegetarian']) == true && $data['vegetarian'] == 'on') ? 1 : 0),
            'status' =>  ((isset($data['status']) && $data['status'] == 'active') ? 1 : 0),
            'role' => ((isset($data['role']) && $data['role'] == 'admin') ? 'admin' : 'user')
        ]);
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('auth.register')->with([
            'user' => new User(),
            'header' => 'Register new user',
            'btnText' => 'Add new user',
            'edit' => false]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->create($request->all());

        return redirect($this->redirectPath());
    }

    /**
     * Returns view for editing your information or
     * for admin to edit some user information
     * @id id of the user
     */
    public function getEdit($id){
        $user = User::find($id);
        if($user != null) {
            return view('auth.register')
                ->with(['user' => $user,
                        'header' => "Edit <b>".$user->first_name." ".$user->last_name."</b> 's profile",
                        'btnText' => "Update",
                        'edit' => true]);
        }
        else
            \App::abort(404);
    }

    public function postEdit(Request $request, $id){
        $validator = $this->updateValidator($request->all(), $id);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->update($request->all(), $id);

        return redirect($this->redirectPath());

    }

    public function getEditMyProfile(){

        $user=Auth::user();
        if($user != null) {
            return view('auth.register')
                ->with(['user' => $user,
                        'header' => "Edit your profile",
                        'btnText' => 'Save changes',
                        'edit' => true]);
        }
        else
            \App::abort(404);

    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            if(\Auth::user()->status == 1){
            return $this->handleUserWasAuthenticated($request, $throttles);
            }
            else
            {
                Auth::logout();
                return redirect($this->loginPath())
                    ->withInput($request->only($this->loginUsername(), 'remember'))
                    ->withErrors([
                        $this->loginUsername() => 'Your account is not active!',
                    ]);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }



}
