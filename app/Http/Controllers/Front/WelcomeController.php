<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function getWelcomeView()
    {
        return view('front-end.welcome.welcome-view');
    }

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getWelcomeView']);
    }
}
