<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\MakeOrderRequest;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use App\Models\Time;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function getThisWeekOrder()
    {
        $thisweek = strtotime('this week');
        $dates = array(
            date('Y-m-d', strtotime('monday', $thisweek)),
            date('Y-m-d', strtotime('tuesday', $thisweek)),
            date('Y-m-d', strtotime('wednesday', $thisweek)),
            date('Y-m-d', strtotime('thursday', $thisweek)),
            date('Y-m-d', strtotime('friday', $thisweek))
        );

        $user = Auth::user();

        $orderMeals = array();
        foreach ($dates as $date) {
            $pom_niza = array();
            $orders = Order::where(['user_id' => $user->id, 'date' => $date])->get();
            if (count($orders) != 0) {
                $pom_niza = array(($orders->first()->time->time) => $orders);
            }
            $orderMeals[date('D, d M Y', strtotime($date))] = $pom_niza;
        }
        return view('front-end.orders.show-orders')->with([
            'days'     => $orderMeals,
            'nextweek' => false
        ]);
    }


    public function getNextWeekOrder()
    {
        $nextweek = strtotime('next week');
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('tuesday', $nextweek)),
            date('Y-m-d', strtotime('wednesday', $nextweek)),
            date('Y-m-d', strtotime('thursday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );

        $user = Auth::user();

        $orderMeals = array();
        $invalidOrders = array();
        foreach ($dates as $date) {
            $pom_niza = array();
            $orders = Order::where(['user_id' => $user->id, 'date' => $date])->get();
            $dayName = strtolower(date('l', strtotime($date)));
            $invalidOrder = Order::where(['user_id' => $user->id, 'date' => $date])
                            ->whereHas('item', function ($query) use ($dayName) {
                                return $query->where($dayName, 0)
                                             ->orWhere('status', 0);
                            })->pluck('item_id');
            if (count($orders) != 0) {
                $pom_niza = array(($orders->first()->time->time) => $orders);
            }
            $orderMeals[date('D, d M Y', strtotime($date))] = $pom_niza;
            $invalidOrders[date('D, d M Y', strtotime($date))] = $invalidOrder;
        }
        return view('front-end.orders.show-orders')->with([
            'days'     => $orderMeals,
            'invalid'  => $invalidOrders,
            'nextweek' => true
        ]);
    }

    public function getMakeOrder()
    {
        $nextweek = strtotime('next week');
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('tuesday', $nextweek)),
            date('Y-m-d', strtotime('wednesday', $nextweek)),
            date('Y-m-d', strtotime('thursday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );

        $categories = Category::where('status', 1)->lists('name', 'id');
        $firstCategory = key(reset($categories));
        $monday = Item::where(['status' => 1, 'monday' => 1, 'category_id' => $firstCategory])->get();
        $tuesday = Item::where(['status' => 1, 'tuesday' => 1, 'category_id' => $firstCategory])->get();
        $wednesday = Item::where(['status' => 1, 'wednesday' => 1, 'category_id' => $firstCategory])->get();
        $thursday = Item::where(['status' => 1, 'thursday' => 1, 'category_id' => $firstCategory])->get();
        $friday = Item::where(['status' => 1, 'friday' => 1, 'category_id' => $firstCategory])->get();

        $user=Auth::user();

        if($user->vegetarian)
        {
            $monday=$monday->where('vegetarian', 1);
            $tuesday=$tuesday->where('vegetarian', 1);
            $wednesday=$wednesday->where('vegetarian', 1);
            $thursday=$thursday->where('vegetarian', 1);
            $friday=$friday->where('vegetarian', 1);
        }

        $timeLunch = Time::where(['status' => 1])->get();

        for ($i = 0; $i < 5; $i++) {
            $temp=array();
            foreach ($timeLunch as $time) {
                if (Order::where([
                        'date'      => $dates[$i],
                        'termin_id' => $time->id
                    ])->distinct(['user_id'])->get(['user_id'])->count() < $time->max_people) {
                    $temp[$time->id] = date('H:i', strtotime($time->time));
                }
                $times[$i] = $temp;
            }
        }

        return view('front-end.orders.make-order')
            ->with([
                'categories'  => $categories,
                'monday'      => $monday,
                'tuesday'     => $tuesday,
                'wednesday'   => $wednesday,
                'thursday'    => $thursday,
                'friday'      => $friday,
                'times'       => $times
            ]);

    }

    public function postMakeOrder(MakeOrderRequest $request)
    {
        $this->deleteNextOrder(\Input::get('week'));
        $nextweek = strtotime(\Input::get('week'));
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('tuesday', $nextweek)),
            date('Y-m-d', strtotime('wednesday', $nextweek)),
            date('Y-m-d', strtotime('thursday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );

        $meals = array(
            $dates[0] => [\Input::get("monday-meals") => Time::where('time', \Input::get("monday-time"))->pluck('id')],
            $dates[1] => [\Input::get("tuesday-meals") => Time::where('time', \Input::get("tuesday-time"))->pluck('id')],
            $dates[2] => [\Input::get("wednesday-meals") => Time::where('time', \Input::get("wednesday-time"))->pluck('id')],
            $dates[3] => [\Input::get("thursday-meals") => Time::where('time', \Input::get("thursday-time"))->pluck('id')],
            $dates[4] => [\Input::get("friday-meals") => Time::where('time', \Input::get("friday-time"))->pluck('id')]
        );
        $user = Auth::user();


        foreach ($meals as $date => $meal) {
            foreach ($meal as $food => $time) {
                if ($food != null) {
                    $foods = explode(',', $food);
                    foreach ($foods as $food) {
                        if ($food != "") {
                            Order::create([
                                'date'      => $date,
                                'user_id'   => $user->id,
                                'item_id'   => $food,
                                'termin_id' => $time
                            ])->save();
                        }
                    }
                }
            }
        }
        return \Redirect::to('/next-week-order');
    }

    public function deleteNextOrder($week)
    {
        $user = Auth::user();
        if($week != null) {
            $nextweek = strtotime($week);
        }
        else
        {
            $nextweek = strtotime('next week');
        }
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );

        $order = Order::where(['user_id' => $user->id])->whereBetween('date', array($dates[0], $dates[1]))->delete();
        if ($order) {
            return response()->json(['response' => 200, 'message' => 'Successfully activated'], 200);
        } else {
            return response()->json(['error' => 404, 'message' => 'Not found'], 404);
        }
    }

    public function getChangeOrder($day)
    {
        $date = date('Y-m-d', strtotime($day));
        $categories = Category::where('status', 1)->lists('name', 'id');
        $firstCategory = key(reset($categories));
        $weekday = strtolower(date('l', strtotime($date)));
        $monday = Item::where(['status' => 1, $weekday => 1, 'category_id' => $firstCategory])->get();
        $times = Time::where(['status' => 1])->lists('time', 'id');
        $user = Auth::user();
        $order = Order::where(['user_id' => $user->id, 'date' => $date])->get();

        return view('front-end.orders.change-day-order')
            ->with([
                'categories' => $categories,
                'monday'     => $monday,
                'times'      => $times,
                'order'      => $order
            ]);
    }

    public function postChangeOrder($day)
    {
        $user = Auth::user();
        $date = date('Y-m-d', strtotime($day));
        Order::where(['user_id' => $user->id, 'date' => $date])->delete();

        $meals = \Input::get("monday-meals");
        $time = \Input::get("monday-time");
        $user = Auth::user();

        if ($meals != null) {
            $foods = explode(',', $meals);
            foreach ($foods as $food) {
                if ($food != "") {
                    Order::create([
                        'date'      => $date,
                        'user_id'   => $user->id,
                        'item_id'   => $food,
                        'termin_id' => $time
                    ])->save();
                }
            }
        }

        return \Redirect::to('/next-week-order');
    }

    public function getCategoryItems($category, $day)
    {
        $items = Item::where(['status' => 1, 'category_id' => $category, $day => 1])->get();
        if(Auth::user()->vegetarian)
        {
            $items=$items->where('vegetarian', 1);
        }
        return $items;
    }

    public function getTimes($week)
    {
        $nextweek = strtotime($week);
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('tuesday', $nextweek)),
            date('Y-m-d', strtotime('wednesday', $nextweek)),
            date('Y-m-d', strtotime('thursday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );

        $timeLunch = Time::where(['status' => 1])->get();
        $times =array();
        for ($i = 0; $i < 5; $i++) {
            $temp = array();
            foreach ($timeLunch as $time) {
                if (Order::where([
                        'date'      => $dates[$i],
                        'termin_id' => $time->id
                    ])->distinct(['user_id'])->get(['user_id'])->count() < $time->max_people) {
                    $temp[$time->id] = date('H:i', strtotime($time->time));
                }
                $times[$i] = $temp;
            }
        }

        return $times;
    }
}
