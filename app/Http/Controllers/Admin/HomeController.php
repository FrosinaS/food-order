<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Items;

class HomeController extends Controller {
    
    public $model;
    public function __construct(){
        
        $this->model=new Items;
        $this->middleware('auth',['except'=>'register']);
    }
    
    public function index() {
        return view('admin.home.home');
    }

    public function categories() {
        
       return view('admin.category.index');
    }
    public function items() {
         //get all items
        
        $items=  $this->model->getAll();
        return view('admin.items.index', ['items' => $items]);
    }
    public function pending() {
         
       return redirect('register');
    }
    public function report() {
         
       return redirect('register');
    }
    
}
?>