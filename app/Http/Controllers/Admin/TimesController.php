<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Time;
use \Input;
use App\Http\Requests\AddNewTimeRequest;

class TimesController extends Controller
{
    public function getTimes()
    {
        $times=Time::all();
        return view('admin.times.list')->with('times', $times);
    }


    public function addNewTime()
    {
        return view('admin.times.new-time')
                    ->with(['time' => new Time(),
                            'header' => 'Add new time',
                            'edit' => false,
                            'btnText'=> 'Save']);
    }

    public function saveNewTime(AddNewTimeRequest $request)
    {
        $time=Time::create(['time'=> Input::get('time'),
                            'max_people' => Input::get("max-people"),
                            'status' => (Input::get('status') == "active") ? 1 :0]);

        if($time)
        {
            return \Redirect::to('/admin/times');
        }

    }

    public function updateTime(AddNewTimeRequest $request, $id)
    {
        $time = Time::find($id)->update(['time'=> Input::get('time'),
                                         'max_people' => Input::get("max-people"),
                                         'status' => (Input::get('status') == "active") ? 1 : 0]);

        if($time)
        {
            return \Redirect::to('/admin/times');
        }

    }

    public function editTime($id)
    {
        return view('admin.times.new-time')
                    ->with(['time' => Time::find($id),
                            'header' => 'Edit time',
                            'edit' => true,
                            'btnText'=> 'Update']);
    }

    public function activateTimes()
    {
        $timeIds=Input::get('timeIds');
        $success = 0;
        foreach ($timeIds as $id) {
            $success = Time::where('id', $id)->update(['status' => 1]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deactivateTimes()
    {
        $timeIds=Input::get('timeIds');
        $success = 0;
        foreach ($timeIds as $id) {
            $success = Time::where('id', $id)->update(['status' => 0]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deleteTimes()
    {
        $timeIds=Input::get('timeIds');
        $success = 0;
        foreach ($timeIds as $id) {
            $success = Time::where('id', $id)->delete();
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully deleted' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }
}
