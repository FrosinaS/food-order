<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller {

    public $model;

    function __construct() {

        $this->model = new Category;
        $this->data = Input::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        
        list($where, $order_by, $order_type, $like_column, $like) = $this->check_filters();

        $ee = $this->check_filters();
        $this->model = new Category;
        $categories = $this->model->getAllFilter($where, $order_by, $order_type, $like_column, $like);
        //$categories = $this->model->getAll();
        return view('admin/category/index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
        return view('admin/category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        
        $this->model->saveCategory($this->data);
        return redirect('admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
        $category = $this->model->getCategory($id);
        return view('admin/category/edit', [ 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
        $category = $this->model->updateCategory($id, $this->data);
        return redirect('admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function delete() {

        $items = $this->data;
        $c = 0;
        foreach ($items as $item) {
            if ($c > 0) {
                $this->model->deleteCategory($item);
                $c++;
            } else
                $c++;
        }
        return redirect('admin/categories');
    }

    public function activate() {
        $items = $this->data;
        $c = 0;
        foreach ($items as $item) {
            if ($c > 0 && $item != 0) {
                $this->model->setStatus($item, 1);
                $c++;
            } else
                $c++;
        }

        return redirect('admin/categories');
    }

    public function deactivate() {
        $items = $this->data;
        $c = 0;
        foreach ($items as $item) {
            if ($c > 0 && $item != 0) {
                $this->model->setStatus($item, 0);
                $c++;
            } else
                $c++;
        }
        return redirect('admin/categories');
    }

    private function check_filters() {

        $where = array();
        $like_column = FALSE;
        $like = FALSE;
        $order_by = FALSE;
        $order_type = FALSE;



        //check if there is filter for status
        if (Input::get('category_status')) {
            switch (Input::get('category_status')) {
                case 'active':
                    $where['category_status'] = '1';
                    break;
                case 'not_active':
                    $where['category_status'] = '0';
                    break;
            }
        }

        //check if there is filter for created_at
        if (Input::get('created')) {
            $order_by = 'created_at';
            switch (Input::get('created')) {
                case 'recent':
                    $order_type = 'desc';
                    break;

                case 'old':
                    $order_type = 'asc';
                    break;
            }
        }
        if (Input::get('search')) {
//            $like = trim(Input::get('search'));
            $like = Input::get('search');
            $like_column = 'category_name';
        }
//        else {
//            $like="Torti";
//            $like_column="category_name";
//        }

        return array(
            $where,
            $order_by,
            $order_type,
            $like_column,
            $like
        );
    }

}
