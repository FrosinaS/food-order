<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use \Input;

class UsersController extends Controller
{
    public function getUsers()
    {
        return view('admin.users.list')->with('users', User::all());
    }

    public function searchUsers($search=null)
    {
        $users = User::where('users.username','LIKE', "%$search%")
                    ->orWhere('users.first_name', 'LIKE', "%$search%")
                    ->orWhere('users.last_name', 'LIKE', "%$search%")->get();
        return view('admin.users.list')->with(['users' => $users]);
    }

    public function activateUsers()
    {
        $userIds=Input::get('userIds');
        $success = 0;
        foreach ($userIds as $id) {
           $success = User::where('id', $id)->update(['status' => 1]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deactivateUsers()
    {
        $userIds=Input::get('userIds');
        $success = 0;
        foreach ($userIds as $id) {
           $success = User::where('id', $id)->update(['status' => 0]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully deactivated' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deleteUsers()
    {
        $userIds=Input::get('userIds');
        $success = 0;
        foreach ($userIds as $id) {
            $success = User::where('id', $id)->delete();
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully deleted' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }
}
