<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\AddCategoryRequest;
use \Input;

class CategoriesController extends Controller
{
    public function getCategories()
    {
        $categories = Category::all();
        return view('admin.categories.list')->with('categories', $categories);
    }

    public function searchCategories($search=null)
    {
        $categories = Category::where('categories.name','LIKE', "%$search%")->get();
        return view('admin.categories.list')->with(['categories' => $categories]);
    }

    public function addNewCategory()
    {
        return view('admin.categories.new-category')
                    ->with(['category' => new Category(),
                            'header' => 'Add new category',
                            'edit' => false,
                            'btnText' => 'Save']);
    }

    public function saveNewCategory(AddCategoryRequest $request)
    {
        $success = Category::create(['name' => Input::get('name'),
                                     'description' => Input::get('description'),
                                     'status' => (Input::get('status') == 'active') ? 1 : 0]);
        if($success)
        {
            return \Redirect::to('/admin/categories');
        }
    }

    public function editCategory($id)
    {
        $category = Category::find($id);
        return view('admin.categories.new-category')
                    ->with(['category' => $category,
                            'header' => 'Edit '.$category->name.' category',
                            'edit' => true,
                            'btnText' => 'Update']);
    }

    public function updateCategory($id)
    {
        $success = Category::find($id)->update(['name' => Input::get('name'),
                                                'description' => Input::get('description'),
                                                'status' => (Input::get('status') == 'on') ? 1 : 0]);
        if($success)
        {
            return \Redirect::to('/admin/categories');
        }
    }


    public function activateCategories()
    {
        $categoryIds=Input::get('categoryIds');
        $success = 0;
        foreach ($categoryIds as $id) {
            $success = Category::where('id', $id)->update(['status' => 1]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deactivateCategories()
    {
        $categoryIds=Input::get('categoryIds');
        $success = 0;
        foreach ($categoryIds as $id) {
            $success = Category::where('id', $id)->update(['status' => 0]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deleteCategories()
    {
        $categoryIds=Input::get('categoryIds');
        $success = 0;
        foreach ($categoryIds as $id) {
            $success = Category::where('id', $id)->delete();
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully deleted' ], 200);
        }
        else{
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }
}
