<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use App\Http\Requests\AddItemRequest;
use \Input;

class ItemsController extends Controller
{
    public function getItems()
    {
        $items = Item::all();
        return view('admin.items.list')->with('items', $items);
    }

    public function searchItems($search=null)
    {
        $items = Item::where('items.name','LIKE', "%$search%")->get();
        return view('admin.items.list')->with(['items' => $items]);
    }

    public function addNewItem()
    {
        return view('admin.items.new-item')
                    ->with(['item' => new Item(),
                            'categories' => Category::all(),
                            'header' => 'Add new item',
                            'edit' => false,
                            'btnText' => 'Save']);
    }

    public function saveNewItem(AddItemRequest $request)
    {

        $success = Item::create(['name' => Input::get('name'),
                                 'description' => Input::get('description'),
                                 'category_id' => Input::get('category'),
                                 'price' => (Input::get('price')),
                                 'weight' => (Input::get('weight')),
                                 'status' => (Input::get('status') == 'active') ? 1 : 0,
                                 'monday' => (Input::get('monday') === 'on') ? 1 : 0,
                                 'tuesday' => (Input::get('tuesday') === 'on') ? 1 : 0,
                                 'wednesday' => (Input::get('wednesday') === 'on') ? 1 : 0,
                                 'thursday' => (Input::get('thursday') === 'on') ? 1 : 0,
                                 'friday' => (Input::get('friday') === 'on') ? 1 : 0,
                                 'vegetarian' => (Input::get('vegetarian') === 'on') ? 1 : 0]);
        if($success)
        {
            return \Redirect::to('/admin/items');
        }
    }

    public function editItem($id)
    {
        $item = Item::find($id);
        return view('admin.items.new-item')
                    ->with(['item' => $item,
                            'categories' => Category::all(),
                            'header' => 'Edit '.$item->name,
                            'edit' => true,
                            'btnText' => 'Update']);
    }

    public function updateItem($id)
    {
        $success = Item::find($id)->update(['name' => Input::get('name'),
                                            'description' => Input::get('description'),
                                            'price' => (Input::get('price')),
                                            'weight' => (Input::get('weight')),
                                            'status' => (Input::get('status') == 'active') ? 1 : 0,
                                            'monday' => (Input::get('monday') === 'on') ? 1 : 0,
                                            'category_id' => Input::get('category'),
                                            'tuesday' => (Input::get('tuesday') === 'on') ? 1 : 0,
                                            'wednesday' => (Input::get('wednesday') === 'on') ? 1 : 0,
                                            'thursday' => (Input::get('thursday') === 'on') ? 1 : 0,
                                            'friday' => (Input::get('friday') === 'on') ? 1 : 0,
                                            'vegetarian' => (Input::get('vegetarian') === 'on') ? 1 : 0]);
        if($success)
        {
            return \Redirect::to('/admin/items');
        }
    }


    public function activateItems()
    {
        $itemIds=Input::get('itemIds');
        $success = 0;
        foreach ($itemIds as $id) {
            $success = Item::where('id', $id)->update(['status' => 1]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else
        {
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deactivateItems()
    {
        $itemIds=Input::get('itemIds');
        $success = 0;
        foreach ($itemIds as $id) {
            $success = Item::where('id', $id)->update(['status' => 0]);
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully activated' ], 200);
        }
        else
        {
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }

    public function deleteItems()
    {
        $itemIds=Input::get('itemIds');
        $success = 0;
        foreach ($itemIds as $id) {
            $success = Item::where('id', $id)->delete();
        }
        if($success)
        {
            return response()->json([ 'response'=> 200, 'message' => 'Successfully deleted' ], 200);
        }
        else
        {
            return response()->json([ 'error'=> 404, 'message' => 'Not found' ], 404);
        }
    }


}
