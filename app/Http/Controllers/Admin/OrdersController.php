<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Time;
use App\Http\Requests;
use App\Http\Controllers\Controller;
Use App\Models\Item;

class OrdersController extends Controller
{
    public $numTimes, $times, $mealOrders;

    public function getOrders(){
        $orders=Order::all()->where('date', date('Y-m-d'));
        return view('admin.orders.orders-list')->with('orders', $orders);
    }

    public function getOrdersTable()
    {
        $this->numTimes = Time::where('status', 1)->count();
        $this->times = Time::where('status', 1)->orderBy('time', 'asc')->get();

        $nextweek = strtotime('next week');
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );

        $orders=Item::where('items.status', '=', 1)
            ->leftJoin('orders', 'items.id', '=', 'orders.item_id')
            ->leftJoin('times', 'orders.termin_id', '=', 'times.id')
            ->whereBetween('orders.date', array($dates[0], $dates[1]))
            ->groupBy('orders.termin_id', 'orders.date', 'orders.item_id')
            ->orderBy('items.name', 'ASC')
            ->get(['orders.date as date', 'items.name as name', 'times.time as time',\DB::raw('COUNT(orders.item_id) as `numItems`')]);
        $mealOrders=array();

        foreach($orders as $order)
        {
            if(array_key_exists($order->name, $mealOrders))
            {
                if (array_key_exists($order->date, $mealOrders[$order->name]))
                {
                    if (array_key_exists($order->time, $mealOrders[$order->name][$order->date]))
                    {
                        $mealOrders[$order->name][$order->date][$order->time] += $order->numItems;
                    }
                    else
                    {
                        $mealOrders[$order->name][$order->date][$order->time] = $order->numItems;
                    }
                }
                else
                {
                    $mealOrders[$order->name][$order->date][$order->time] = $order->numItems;
                }
            }
            else
            {
                $mealOrders[$order->name][$order->date][$order->time] = $order->numItems;
            }
        }

        $this->mealOrders=$mealOrders;
    }

    public function getTable(){
       $this->getOrdersTable();

        return view('admin.orders.orders-table')
                            ->with(['numTimes' => $this->numTimes,
                                    'orders' => $this->mealOrders,
                                    'times' => $this->times]);
    }


    public function downloadPdf(){

       $this->getOrdersTable();

        $pdf = \PDF::loadView('admin.orders.pdf-table', ['numTimes' => $this->numTimes,
                                                         'orders' => $this->mealOrders,
                                                         'times' => $this->times]);
        return $pdf->download('order-table.pdf');
    }

    public function getWeekOrdersByPeople()
    {
        $nextweek = strtotime('this week');
        $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
        );
        $orders=Order::whereBetween('date', array($dates[0], $dates[1]))->get();
        $userOrders=array();
        foreach($orders as $order)
        {
            if(array_key_exists(($order->user->first_name." ".$order->user->last_name), $userOrders))
            {
                if(array_key_exists($order->date, $userOrders[$order->user->first_name." ".$order->user->last_name])) {
                    if (array_key_exists($order->time->time,
                        $userOrders[$order->user->first_name . " " . $order->user->last_name][$order->date])) {
                        array_push($userOrders[$order->user->first_name . " " . $order->user->last_name][$order->date][$order->time->time],
                            $order->item->name);
                    } else {
                        $userOrders[$order->user->first_name . " " . $order->user->last_name][$order->date][$order->time->time] = array();
                        array_push($userOrders[$order->user->first_name . " " . $order->user->last_name][$order->date][$order->time->time],
                            $order->item->name);
                    }
                }
                else
                {
                    $userOrders[$order->user->first_name . " " . $order->user->last_name][$order->date][$order->time->time] = array();
                    array_push($userOrders[$order->user->first_name . " " . $order->user->last_name][$order->date][$order->time->time],
                        $order->item->name);
                }

            }
            else
            {
                $userOrders[$order->user->first_name." ".$order->user->last_name]=array();
                $userOrders[$order->user->first_name." ".$order->user->last_name][$order->date][$order->time->time]=array();
                array_push($userOrders[$order->user->first_name." ".$order->user->last_name][$order->date][$order->time->time], $order->item->name);

            }
        }
        return view('admin.orders.orders-by-week')->with('orders', $userOrders);
    }

    public function getSummaryReports()
    {
        return view('admin.orders.summary-reports');
    }

    public function getSummaryReportsByDate($start, $end)
    {
        $sum = Order::leftJoin('items', 'orders.item_id', '=', 'items.id')
                    ->whereBetween('date', array($start, $end))
                    ->select(\DB::raw('SUM(items.price) as price'))->get();
        return $sum[0]->price;
    }
}

