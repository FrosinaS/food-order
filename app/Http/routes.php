<?php


Route::get('/','Admin\UsersController@getUsers');

Route::get('/admin','Admin\UsersController@getUsers');

Route::get('/admin/users', 'Admin\UsersController@getUsers');

Route::get('/admin/users/search/{search?}', 'Admin\UsersController@searchUsers');

Route::post('/admin/users/activate', 'Admin\UsersController@activateUsers');

Route::post('/admin/users/deactivate', 'Admin\UsersController@deactivateUsers');

Route::post('/admin/users/delete', 'Admin\UsersController@deleteUsers');



Route::get('admin/times', 'Admin\TimesController@getTimes');

Route::get('admin/times/add-new-time', 'Admin\TimesController@addNewTime');

Route::get('admin/times/edit-time/{id}', 'Admin\TimesController@editTime');

Route::post('admin/times/add-new-time', 'Admin\TimesController@saveNewTime');

Route::post('admin/times/edit-time/{id}', 'Admin\TimesController@updateTime');

Route::post('/admin/times/activate', 'Admin\TimesController@activateTimes');

Route::post('/admin/times/deactivate', 'Admin\TimesController@deactivateTimes');

Route::post('/admin/times/delete', 'Admin\TimesController@deleteTimes');


Route::get('/admin/orders', 'Admin\OrdersController@getOrders');

Route::get('/admin/orders-table', 'Admin\OrdersController@getTable');

Route::get('/admin/pdf-table', 'Admin\OrdersController@downloadPdf');

Route::get('/admin/this-week-order', 'Admin\OrdersController@getWeekOrdersByPeople');

Route::get('/admin/reports', 'Admin\OrdersController@getSummaryReports');

Route::get('/admin/get-reports/{start}/{end}', 'Admin\OrdersController@getSummaryReportsByDate');

Route::get('/admin/categories', 'Admin\CategoriesController@getCategories');

Route::get('/admin/categories/add-new', 'Admin\CategoriesController@addNewCategory');

Route::post('/admin/categories/add-new', 'Admin\CategoriesController@saveNewCategory');

Route::get('/admin/categories/edit-category/{id}', 'Admin\CategoriesController@editCategory');

Route::post('/admin/categories/edit-category/{id}', 'Admin\CategoriesController@updateCategory');

Route::post('/admin/categories/activate', 'Admin\CategoriesController@activateCategories');

Route::post('/admin/categories/deactivate', 'Admin\CategoriesController@deactivateCategories');

Route::post('/admin/categories/delete', 'Admin\CategoriesController@deleteCategories');

Route::get('/admin/categories/search/{search?}', 'Admin\CategoriesController@searchCategories');


Route::get('/admin/items', 'Admin\ItemsController@getItems');

Route::get('/admin/items/add-new', 'Admin\ItemsController@addNewItem');

Route::post('/admin/items/add-new', 'Admin\ItemsController@saveNewItem');

Route::get('/admin/items/edit-item/{id}', 'Admin\ItemsController@editItem');

Route::post('/admin/items/edit-item/{id}', 'Admin\ItemsController@updateItem');

Route::post('/admin/items/activate', 'Admin\ItemsController@activateItems');

Route::post('/admin/items/deactivate', 'Admin\ItemsController@deactivateItems');

Route::post('/admin/items/delete', 'Admin\ItemsController@deleteItems');

Route::get('/admin/items/search/{search?}', 'Admin\ItemsController@searchItems');

Route::get('/my-order', 'Front\HomeController@getThisWeekOrder');

Route::get('/next-week-order', 'Front\HomeController@getNextWeekOrder');

Route::post('/delete-order', 'Front\HomeController@deleteNextOrder');

Route::get('/change-order/{day}', 'Front\HomeController@getChangeOrder');
Route::post('/change-order/{day}', 'Front\HomeController@postChangeOrder');

Route::get('/make-order', 'Front\HomeController@getMakeOrder');

Route::post('/make-order', 'Front\HomeController@postMakeOrder');

Route::get('/get-items/{category}/{day}', 'Front\HomeController@getCategoryItems');

Route::get('/get-times/{week}', 'Front\HomeController@getTimes');

Route::get('/home', 'Front\WelcomeController@getWelcomeView');

Route::controller('auth', 'Auth\AuthController');

Route::controller('password', 'Auth\PasswordController');