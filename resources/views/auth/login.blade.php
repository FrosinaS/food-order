
@extends('front-end.front-main-layout')
@section('main-content')
    <style>
        .checkbox
        {
           top:10px !important;
        }
    </style>
<div class="container-fluid" style="text-align:center;">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default" style="background-color:#1abc9c;">
                <h3>Food Order</h3>
				<div class="panel-heading h4" style="background-color:#1abc9c; border-top:2px solid slategray; border-bottom: none;">Login</div>
				<div class="panel-body" style="background-color:white; border-top:2px solid slategray;">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="login-form col-md-6" style="margin:auto !important; float:none;">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control login-field" value="{{ old('email') }}" placeholder="Enter your email" id="login-name" />
                                <label class="login-field-icon fui-user" for="login-name"></label>
                            </div>

                            <div class="form-group">
                                <input type="password" name="password" class="form-control login-field"  placeholder="Password" id="login-pass" />
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>


                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-5">
                                <div class="checkbox" style="text-align: left; padding-left:10px;">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" >Login</button>
                        <a class="btn btn-link" style="color:#d9534f;" href="{{ url('/password/email') }}">Forgot Your Password?</a>


					</form>
				</div>
			</div>
		</div>
	</div>
    <script>
        $(document).ready(function(){
            $(":checkbox").radiocheck();
        });
    </script>

</div>
@endsection
