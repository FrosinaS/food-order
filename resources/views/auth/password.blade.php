
@extends('front-end.front-main-layout')
@section('main-content')
<div class="container-fluid" style="text-align:center;">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="background-color:#1abc9c;">
                <h3>Food Order</h3>
                <div class="panel-heading h4" style="background-color:#1abc9c; border-top:2px solid slategray; border-bottom: none;">Reset your password</div>
                <div class="panel-body" style="background-color:white; border-top:2px solid slategray;">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif


					<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
