@extends('front-end.front-main-layout')
@section('main-content')

    <div class="container-fluid" style="text-align:center; height: 100% !important;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="background-color:#1abc9c;">
                    <h3>Food Order </h3>

                    <div class="panel-heading h4" style="background-color:#1abc9c; border-top:2px solid slategray; border-bottom: none;">
                        {!! $header !!}

                    </div>
                    <div class="panel-body" style="background-color:white; border-top:2px solid slategray;">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($edit == true)

                             <form class="form-horizontal" role="form" method="POST"
                                    action="/auth/edit/{{$user->id}}" style="margin-top:30px; height: auto !important;">
                        @else

                             <form class="form-horizontal" role="form" method="POST" style="margin-top:30px; height: auto !important;">

                        @endif
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Username: </label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="username" id="username"
                                                       value="{{$user->username}}"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">First name: </label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="first-name"
                                                       value="{{$user->first_name}}"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Last name: </label>

                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="last-name"
                                                       value="{{$user->last_name}}"/>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">E-Mail Address: </label>

                                            <div class="col-md-6">
                                                <input type="email" class="form-control" name="email"
                                                       value="{{$user->email}}">
                                            </div>
                                        </div>
                                        @if(Auth::guest() || (\App\Helpers\UserHelper::isAdmin() && $edit == false))
                                            <div class="form-group row">
                                                <label class="col-md-4 control-label">Password: </label>

                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="password">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 control-label">Confirm Password: </label>

                                                <div class="col-md-6">
                                                    <input type="password" class="form-control"
                                                           name="password_confirmation">
                                                </div>
                                            </div>
                                        @endif
                                        @if(\App\Helpers\UserHelper::isAdmin())
                                            <div class="form-group row">
                                                <label class="col-md-4 control-label">Status: </label>

                                                <div class="col-md-6">
                                                    <select name="status" class="form-control">
                                                        <option value="active">Active</option>
                                                        <option value="not-active">Not active</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 control-label">Role: </label>

                                                <div class="col-md-6">
                                                    <select name="role" class="form-control">
                                                        <option value="user">User</option>
                                                        <option value="admin">Administrator</option>
                                                    </select>
                                                </div>
                                            </div>

                                        @endif

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Vegetarian: </label>

                                            <div class="col-md-6" style="margin-top:10px;">
                                                <label class="checkbox">
                                                    <input type="checkbox" class="checkUser pull-left" name="vegetarian" data-toggle="checkbox"  id="{{$user->id}}" />
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-10">
                                                <button type="submit" class="btn btn-primary pull-right">
                                                    {!! $btnText !!}
                                                </button>
                                            </div>
                                        </div>

                             </form>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {

            $(":checkbox").radiocheck();

            $('select[name^="status"] option:selected').attr("selected", null);
            $('select[name^="status"] option[value="{{$user->status == 1 ? 'active' : 'not-active'}}"]').attr("selected", "selected");

            $('select[name^="role"] option:selected').attr("selected", null);
            $('select[name^="role"] option[value="{{$user->role}}"]').attr("selected", "selected");

            $('input[name^="vegetarian"]').prop("checked", "{{$user->vegetarian == 1 ? true : false}}");
        });
    </script>
@endsection
