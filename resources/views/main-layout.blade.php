<!DOCTYPE html>
<html lang="en">
<head>
    <!-- start: Meta -->
    <meta charset="utf-8">
    <title>Food order</title>
    <meta name="keyword" content="">
    <!-- end: Meta -->
    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->


    <script src="/layouts/Bootstrap_Metro_DashBoard/js/modernizr.js"></script>

    <script src="/layouts/Bootstrap_Metro_DashBoard/js/jquery-1.9.1.min.js"></script>

    <link href="{{asset(URL::to('/css/app.css'))}}" type="text/css">
    <script src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>

    <link href="/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/layouts/Bootstrap_Metro_DashBoard/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css">

    <script src="/bower_components/mixitup/build/jquery.mixitup.min.js"></script>
    <script src="/bower_components/mixitup/build/jquery.mixitup-pagination.js"></script>
    <script src="/bower_components/flat-ui/dist/js/flat-ui.js"></script>
    <script src="/bower_components/flat-ui/js/radiocheck.js"></script>

    <link href="/bower_components/flat-ui/dist/css/flat-ui.css" type="text/css" rel="stylesheet"/>
    <link href="/bower_components/flat-ui/dist/css/flat-ui.css.map" type="text/css" rel="stylesheet"/>
    <link href="/bower_components/flat-ui/docs/assets/css/demo.css" type="text/css" rel="stylesheet"/>
    <link href="/bower_components/flat-ui/less/flat-ui.less" type="text/css" rel="stylesheet"/>
    <link href="/bower_components/flat-ui/less/modules/select.less" type="text/css" rel="stylesheet"/>

    <link href="/bower_components/flat-ui/less/mixins/forms.less" type="text/css" rel="stylesheet"/>

    <script src="/bower_components/flat-ui/dist/js/vendor/html5shiv.js"></script>
    <script src="/bower_components/flat-ui/dist/js/vendor/respond.min.js"></script>

    <script src="/bower_components/flat-ui/dist/js/vendor/video.js"></script>

    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
</head>
<body>
@yield('content')
</body>
</html>
