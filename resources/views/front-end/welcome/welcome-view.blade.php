<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Food order</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css">
    <link rel="stylesheet" href="/css/main.css">

    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet">
</head>
<body>


<div id="depreload" style="background-image:url('https://hdwallpapers.cat/wallpaper_3840x2160/pizza_food_delicious_nice_meal_photo_ultra_3840x2160_hd-wallpaper-1594444.jpg'); opacity:0.7;" class="table">
    <div class="table-cell wrapper">
        <div class="circle">
            <canvas class="line" width="560px" height="560px"></canvas>
            <img src="/images/logo.jpg" class="logo" alt="logo" style="zoom:1.5; border-radius:50%;"/>
        </div>
        <p class="perc"></p>
        <p class="loading">Loading</p>
    </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<link href="/bower_components/animate.css/animate.css" type="text/css" rel="stylesheet"/>
<script src="/js/jquery.DEPreLoad.js"></script>

<script>
    $(document).ready(function() {

        setTimeout(function(){
            $("#depreload .wrapper").animate({ opacity: 1 });
        }, 400);

        setTimeout(function(){
            $("#depreload .logo").animate({ opacity: 1 });
        }, 800);

        var canvas  = $("#depreload .line")[0],
                context = canvas.getContext("2d");

        context.beginPath();
        context.arc(280, 280, 260, Math.PI * 1.5, Math.PI * 1.6);
        context.strokeStyle = '#fff';
        context.lineWidth = 5;
        context.stroke();

        var loader = $("body").DEPreLoad({
            OnStep: function(percent) {
                console.log(percent + '%');

                $("#depreload .line").animate({ opacity: 1 });
                $("#depreload .perc").text(percent + "%");

                if (percent > 5) {
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.beginPath();
                    context.arc(280, 280, 260, Math.PI * 1.5, Math.PI * (1.5 + percent / 50), false);
                    context.stroke();
                }
            },
            OnComplete: function() {
                console.log('Everything loaded!');

                $("#depreload .perc").html("<a href='/auth/login'>Sign in</a> / <a href='/auth/register'>Register</a>");
                $("#depreload .loading").animate({ opacity: 0 });
            }
        });

        $(".circle").hover(function(){
            $(this).addClass('animated flip');
        }, function(){$(this).removeClass('animated flip')})
    });
</script>

<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-56029753-4','auto');ga('send','pageview');
</script>
</body>
</html>