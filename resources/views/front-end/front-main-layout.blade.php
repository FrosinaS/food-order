@extends('main-layout')
@section('content')
    @include('front-end.partials.main-navbar')
    <link href="/bower_components/bootstrap/dist/css/bootstrap-theme.css.map" rel="stylesheet" type="text/css">
      <div>
          @yield('main-content')
        </div>

@endsection