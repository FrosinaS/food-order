@extends('front-end.front-main-layout')
@section('main-content')
    <style>

        @media(max-width: 748px)
        {
            .gold
            {
                margin-top:50px !important;
            }

            .span {
                display: block !important;
                margin:auto !important;
            }
            button
            {
                margin-top:5px;
            }
        }

        body{
            background-color: #DCDCDC !important;
        }

        .meals {
            margin-top: 100px;
        }

        .plat {
            float: left;
            background: lightgoldenrodyellow;
            text-align: center;
            border-radius: 5px;
        }

        .gold {
            float: left;
            position: relative;
            top: -50px;
            padding: 50px 0;
            background: palegoldenrod;
            text-align: center;
            border-radius: 5px;
            padding-left: 20px;
            padding-right: 20px;
        }

        .gold > .price {
            background: rgba(255, 255, 255, 1);
            color: rgba(0, 0, 0, .7);
        }

        .gold > h1, .gold > h2, .gold > p, .gold > span {
            color: rgba(255, 255, 255, 1);
        }

        .meals h1 {
            margin: 20px 0 10px 0;
            font-size: 1.25em;
            color: rgba(0, 0, 0, 0.8);
        }

        .meals h2 {
            font-size: .75em;
            color: rgba(0, 0, 0, .6);
            font-weight: 100;
            letter-spacing: 1px;
        }

        .meals p {
            color: rgba(0, 0, 0, .4);
            margin: 10px 0;
            font-weight: 100;
            font-size: .75em;
        }

        .meals span {
            margin-bottom: 20px;
            padding-bottom: 10px;
            display: inline-block;
            width: 125px;
            font-size: 1em;
            font-weight: 700;
            letter-spacing: 1px;
            color: rgba(0, 0, 0, .5);
            border-bottom: 1px solid rgba(0, 0, 0, .1);
        }

        .meals .price {
            height: 100px;
            width: 100px;
            text-align: center;
            background-color: rgba(231, 76, 60, 1.0);
            border-radius: 50%;
            line-height: 100px;
            color: #fff;
            font-size: 1.5em;
            font-weight: 100;
            margin: 20px auto;
        }

        .deleteOrder {
            margin-bottom: 30px;
        }

    </style>
    @if($nextweek == true)
        <div class="col-md-12">
            <button class="btn btn-danger pull-right deleteOrder">Delete order</button>
        </div>
    @endif

    <div class='meals'>
        @foreach($days as $day => $time)
            <div class='plat col-md-2 col-xs-12 col-sm-12 <?php if (date('D, d M Y') == $day) {
                echo 'gold';
            }?>' style="margin:20px;">
                <h1>{{$day}}</h1>
                @if($time != null)
                    <div class='price'><b class="glyphicon glyphicon-time"
                                          style="font-size:14px;"></b> {{date('H:i', strtotime(key($time)))}}</div>
                    @if($nextweek == true)
                        @if(count($invalid[$day]) > 0)
                            <p style="font-size:10px; font-weight: bolder;">*you should change your order.</p>
                        @endif
                    @endif
                    @foreach($time as $hour => $meals)
                        @foreach($meals as $mealItem)
                            @if($nextweek == true)
                                @if(in_array($mealItem->item->id, (array)$invalid[$day]))
                                    <span class="alert alert-danger span">
                                        {{$mealItem->item->name}}
                                    </span>
                                @else
                                    <span class="span">
                                    {{$mealItem->item->name}}
                                    </span>
                                @endif
                            @else
                                <span class="span">
                                    {{$mealItem->item->name}}
                                    <small>({{$mealItem->item->description}}, {{$mealItem->item->weight}}gr)</small>
                                </span>
                            @endif
                        @endforeach
                    @endforeach
                    @if($nextweek == true)
                    <button class="btn btn-info changeOrder" style="margin-bottom: 10px;" id='{{date('Y-m-d', strtotime($day))}}'>Change order</button>
                @endif
                    @else
                    <div class='price'>/</div>
                @endif
            </div>
        @endforeach
    </div>
    @include("admin.partials.modal")
    <script>
        $(document).ready(function () {

            $(".changeOrder").click(function(){
                window.location.replace('/change-order/'+$(this).attr('id'));
            });

            $(".deleteOrder").click(function () {
                $(".modal-title").html("Delete order");
                $(".modal-body").html("Are you sure you want to delete this order?");
                $("#basicModal").modal();
            });

            $("#no").click(function () {
                $("#basicModal").modal('hide');
            });

            $("#yes").click(function () {
                $.ajax({
                    url: '/delete-order',
                    method: 'POST',
                    data: {
                        _token: '{{ csrf_token()}}'
                    },
                    success: function () {
                        window.location.replace('/make-order');
                    }
                });
            });
        })
    </script>
@endsection