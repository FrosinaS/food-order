@extends('front-end.front-main-layout')
@section('main-content')
    <script src="/js/DiagonalSlider.js" type="application/javascript"></script>
    <script src="bower_components/jquery-ui/jquery-ui.js"></script>
    <link href="/css/reset.css" type="text/css" rel="stylesheet"/>
    <link href="/css/styles.css" type="text/css" rel="stylesheet"/>
    <link href="/css/example.css" type="text/css" rel="stylesheet"/>
    <link href="/bower_components/jquery-ui/themes/base/all.css" type="text/css"/>
    <script src="/bower_components/fieldChooser/fieldChooser.js"></script>

    <link href="/bower_components/animate.css/animate.css" type="text/css" rel="stylesheet"/>
    <style>
        .badge
        {
            background-color: #5bc0de;
            font-size:10px;
            vertical-align: middle !important;
        }

        .ranges ul
        {
            width:auto;
            height:auto;
            overflow:hidden;
        }
        .applyBtn, .cancelBtn
        {
            display: none;
        }

        .circle {
            position: relative;
            display: block;
            margin: auto;
            margin-top:10px;
            background-color: transparent;
            color: navajowhite;
            text-align: center;
            font-size: 11px;
            width: 50%;
            height: auto;
        }

        .circle:after {
            display: block;
            padding-bottom: 100%;
            width: 100%;
            height: 0;
            border-radius: 50%;
            content: "";
            background-color: #5bc0de;
        }

        .circle__inner {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .circle__wrapper {
            display: table;
            width: 100%;
            height: 100%;
        }

        .circle__content {
            display: table-cell;
            vertical-align: top;
            padding-top: 45%;
            text-align: center;
        }


        .gallery_item div {
            height: auto;
        }
        .gallery_content
        {
            margin-left:-50px;
        }

        .content_title
        {
            margin-top: 0px !important;
            position: static;
        }

        .items, .meals {
            color: black !important;
            overflow-x: hidden !important;

        }
        .meals
        {
            overflow-y:hidden !important;
        }

        .order ul li {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        select
        {
            margin-bottom:10px !important;
        }
        .ranges li:last-child { display: none; }
        .left, .right{
            display:none !important;
        }

    </style>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
<form method="POST">
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

    <input type="hidden" class="week" name="week"/>

    <div class="content_slider">
        <div class="content_title" data-default-text="Next week order">
            <div class="text">
            </div>

            <div id="reportrange" class="pull-right" style="margin:auto !important;background: #fff; border-radius: 5px; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;color:darkgray;">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {

                function cb(start, end, label) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $('.content_title').attr("data-default-text", label+' order');
                    $('.content_title .text').html(label+' order');

                    if(label == 'This week')
                    {
                        $('.week').val('this week');
                    }
                    else if(label == 'Next week')
                    {
                        $('.week').val('next week');
                    }
                    else if(label == 'Two weeks after')
                    {
                        $('.week').val('+1 week');
                    }
                    else if(label == 'Three weeks after')
                    {
                        $('.week').val('+2 week');
                    }
                    else if(label == 'Four weeks after')
                    {
                        $('.week').val('+3 week');
                    }
                    else if(label == 'Five weeks after')
                    {
                        $('.week').val('+4 week');
                    }

                    $.ajax({
                        url: '/get-times/' + $('.week').val(),
                        method: 'GET',
                        success: function (data) {
                            $.each(data[0], function(key, value) {
                                $("select[name='monday-time']").empty();
                                $("select[name='monday-time']").append($("<option></option>")
                                        .attr("id", key).text(value));
                            });

                            $("select[name='tuesday-time']").empty();
                            $.each(data[1], function(key, value) {
                                $("select[name='tuesday-time']").append($("<option></option>")
                                        .attr("id", key).text(value));
                            });

                            $("select[name='wednesday-time']").empty();
                            $.each(data[2], function(key, value) {
                                $("select[name='wednesday-time']").append($("<option></option>")
                                        .attr("id", key).text(value));
                            });

                            $("select[name='thursday-time']").empty();
                            $.each(data[3], function(key, value) {
                                $("select[name='thursday-time']").append($("<option></option>")
                                        .attr("id", key).text(value));
                            });

                            $("select[name='friday-time']").empty();
                            $.each(data[4], function(key, value) {
                                $("select[name='friday-time']").append($("<option></option>")
                                        .attr("id", key).text(value));
                            });
                        }
                    });


                }
                cb(moment().startOf('week').add(1, 'week').add(1, 'day'), moment().endOf('week').add(1, 'week').add(1,  'day'), 'Next week');

                $('#reportrange').daterangepicker({
                    'ranges': {
                        'This week': [moment().startOf('week').add(1, 'day'), moment().endOf('week')],
                        'Next week': [moment().add(1, 'week').startOf('week').add(1, 'day'), moment().add(1, 'week').endOf('week').add(1, 'day')],
                        'Two weeks after': [moment().add(2, 'week').startOf('week').add(1, 'day'), moment().add(2, 'week').endOf('week').add(1, 'day')],
                        'Three weeks after': [moment().add(3, 'week').startOf('week').add(1, 'day'), moment().add(3, 'week').endOf('week').add(1, 'day')],
                        'Four weeks after': [moment().add(4, 'week').startOf('week').add(1, 'day'), moment().add(4, 'week').endOf('week').add(1, 'day')],
                        'Five weeks after': [moment().add(5, 'week').startOf('week').add(1, 'day'), moment().add(5, 'week').endOf('week').add(1, 'day')],
                    }
                }, cb);

            });
        </script>

        <div class="gallery_content" style="margin-top:20px;">
            <div class="gallery_item">
                <div data-title="Monday" class="monday">
                    {!! Form::select('monday', $categories, null, ['class' => 'form-control']) !!}

                    <div class="order row " tabindex="1" style="margin:10px;">
                        <label class="label label-info col-md-12" style="margin-bottom: 10px;" >Choose meals:</label>
                        <ul class="items col-md-6 col-xs-6 col-sm-6">
                            @foreach($monday as $meal)
                                <li class="list-group-item" id="{{$meal->id}}" price="{{$meal->price}}">{{$meal->name}}<span class="badge">MKD  {{$meal->price}}</span></li>

                            @endforeach
                        </ul>
                        <ul class="list-group meals col-md-6 col-xs-6 col-sm-6">
                        </ul>
                    </div>
                    <div class="row" style="margin: 10px;">
                        <label class="label label-info col-md-12" style="margin-top: 30px; margin-bottom: 10px;" >Choose time:</label>

                        {!! Form::select('monday-time', $times[0], null, ['class' => 'form-control col-md-6']) !!}
                    </div>

                </div>
            </div>
            <div class="gallery_item">
                <div data-title="Tuesday" class="tuesday">
                    {!! Form::select('tuesday', $categories, $categories, ['class' => 'form-control']) !!}


                    <div class="order row " tabindex="1" style="margin:10px;">
                        <label class="label label-info col-md-12" style="margin-bottom: 10px;" >Choose meals:</label>
                        <ul class="items col-md-6 col-xs-6 col-sm-6">
                            @foreach($tuesday as $meal)
                                <li class="list-group-item" id="{{$meal->id}}" price="{{$meal->price}}">{{$meal->name}}<span class="badge">MKD  {{$meal->price}}</span></li>
                            @endforeach
                        </ul>
                        <ul class="list-group meals col-md-6 col-xs-6 col-sm-6">
                        </ul>
                    </div>
                    <div class="row" style="margin: 10px;">
                        <label class="label label-info col-md-12" style="margin-top: 30px; margin-bottom: 10px;" >Choose time:</label>

                        {!! Form::select('tuesday-time', $times[1], null, ['class' => 'form-control col-md-6']) !!}
                    </div>

                </div>
            </div>
            <div class="gallery_item">
                <div data-title="Wednesday" class="wednesday">
                    {!! Form::select('wednesday', $categories, null, ['class' => 'form-control']) !!}

                    <div class="order row " tabindex="1" style="margin:10px;">
                        <label class="label label-info col-md-12" style="margin-bottom: 10px;" >Choose meals:</label>
                        <ul class="items list-group col-md-6 col-xs-6 col-sm-6">
                            @foreach($wednesday as $meal)
                                <li class="list-group-item" id="{{$meal->id}}" price="{{$meal->price}}">{{$meal->name}}<span class="badge">MKD {{$meal->price}}</span></li>
                            @endforeach
                        </ul>
                        <ul class="list-group meals col-md-6 col-xs-6 col-sm-6">
                        </ul>
                    </div>
                    <div class="row" style="margin: 10px;">
                        <label class="label label-info col-md-12" style="margin-top: 30px; margin-bottom: 10px;" >Choose time:</label>

                        {!! Form::select('wednesday-time', $times[2], null, ['class' => 'form-control col-md-6']) !!}
                    </div>


                </div>


            </div>
            <div class="gallery_item">
                <div data-title="Thursday" class="thursday">
                    {!! Form::select('thursday', $categories, null, ['class' => 'form-control']) !!}


                    <div class="order row " tabindex="1" style="margin:10px;">
                        <label class="label label-info col-md-12" style="margin-bottom: 10px;" >Choose meals:</label>
                        <ul class="items col-md-6 col-xs-6 col-sm-6">
                            @foreach($thursday as $meal)
                                <li class="list-group-item" id="{{$meal->id}}" price="{{$meal->price}}">{{$meal->name}}<span class="badge">MKD {{$meal->price}}</span></li>

                            @endforeach
                        </ul>
                        <ul class="list-group meals col-md-6 col-xs-6 col-sm-6">
                        </ul>
                    </div>
                    <div class="row" style="margin: 10px;">
                        <label class="label label-info col-md-12" style="margin-top: 30px; margin-bottom: 10px;" >Choose time:</label>

                        {!! Form::select('thursday-time', $times[3], null, ['class' => 'form-control col-md-6']) !!}
                    </div>

                </div>
            </div>
            <div class="gallery_item">
                <div data-title="Friday" class="friday">
                    {!! Form::select('friday', $categories, null, ['class' => 'form-control']) !!}

                    <div class="order row " tabindex="1" style="margin:10px;">
                        <label class="label label-info col-md-12" style="margin-bottom: 10px;" >Choose meals:</label>
                        <ul class="items col-md-6 col-xs-6 col-sm-6">
                            @foreach($friday as $meal)
                                <li class="list-group-item" id="{{$meal->id}}" price="{{$meal->price}}">{{$meal->name}}<span class="badge">MKD {{$meal->price}}</span></li>

                            @endforeach
                        </ul>
                        <ul class="list-group meals col-md-6 col-xs-6 col-sm-6">
                        </ul>
                    </div>
                    <div class="row" style="margin: 10px;">
                        <label class="label label-info col-md-12" style="margin-top: 30px; margin-bottom: 10px;" >Choose time:</label>

                        {!! Form::select('friday-time', $times[4], null, ['class' => 'form-control col-md-6']) !!}
                    </div>

                </div>
            </div>

        </div>
        <div class="row col-md-12" style="margin-top:20px;">
            <button type="submit" class="btn btn-info pull-right placeOrder">Make order</button>
        </div>

    </div>
    <input type="hidden" class="monday-mealsIds" name="monday-meals"/>
    <input type="hidden" class="tuesday-mealsIds" name="tuesday-meals"/>
    <input type="hidden" class="wednesday-mealsIds" name="wednesday-meals"/>
    <input type="hidden" class="thursday-mealsIds" name="thursday-meals"/>
    <input type="hidden" class="friday-mealsIds" name="friday-meals"/>

</form>
    <script>
        ;
        (function ($) {

            function addMeals(day, container)
            {
                var meals, mealIds;
                meals = $(day+" .meals li");
                mealIds="";
                meals.each(function(){
                    mealIds+=$(this).attr('id')+",";
                });
                $(container).val(mealIds);
            }

            $(".placeOrder").click(function(){
                 //monday
                addMeals('.monday', '.monday-mealsIds');
                //tuesday
                addMeals('.tuesday', '.tuesday-mealsIds');
                //wednesday
                addMeals('.wednesday', '.wednesday-mealsIds');
                //thursday
                addMeals('.thursday', '.thursday-mealsIds');
                //friday
                addMeals('.friday', '.friday-mealsIds');
            });

            $('.badge').hide();

            $(".gallery_item").hover(
                    function(){
                        $(this).find('.badge').show();
                    },
                    function(){ $(this).find('.badge').hide();})

            $('.gallery_content').createDiagonalSlider();

            $('select').change(function () {
                var day = $(this).attr('name');
                var category = $(this).val();

                $.ajax({
                    url: '/get-items/' + category + '/' + day,
                    method: 'GET',
                    success: function (data) {
                        if ($("." + day + " .items").length > 0) {
                            $("." + day + " .items li").each(function(){$(this).removeClass('zoomInUp').removeClass('zoomInUp').addClass('animated rotateOut')});
                            var items = $("." + day + " .items li");
                            setTimeout(function(){items.remove();}, 500);
                        }
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            $("." + day + " .items").append($("<li>", {"id": data[i].id, "html": data[i].name + "<span class='badge'>MKD  "+data[i].price+"</span>", "price": data[i].price}).addClass('list-group-item zoomInUp animated'));
                        }


                    }
                });

            });

            $(".order").each(function () {
                var container = $(this);
                $(this).fieldChooser(container.find(".items"), container.find('.meals'))
            });

            $(".meals").hover(
                       function(){$(".bounceOut").remove()
                        if($(this).children.length > 0){
                         $(this).append("<div class='circle animated bounceIn'> " +
                            "<div class='circle__inner'> " +
                            "<div class='circle__wrapper'> " +
                            "<div class='circle__content'>Drop items here</div> " +
                            "</div> </div> </div>");}},
                    function(){$(".bounceIn").addClass('bounceOut').removeClass('bounceIn');
                    setTimeout(function(){$(".bounceOut").remove()}, 500);})

            $('#yes').click(function(){$('#basicModal').modal('hide');})
        })(jQuery);
    </script>
@include('front-end.partials.modal')
@endsection