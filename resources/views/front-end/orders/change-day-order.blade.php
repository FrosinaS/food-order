@extends('front-end.front-main-layout')
@section('main-content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/DiagonalSlider.js" type="application/javascript"></script>
    <script src="/bower_components/jquery-ui/jquery-ui.js"></script>
    <link href="/css/reset.css" type="text/css" rel="stylesheet"/>
    <link href="/css/styles.css" type="text/css" rel="stylesheet"/>
    <link href="/css/example.css" type="text/css" rel="stylesheet"/>
    <link href="/bower_components/jquery-ui/themes/base/all.css" type="text/css"/>
    <script src="/bower_components/fieldChooser/fieldChooser.js"></script>
    <link href="/bower_components/animate.css/animate.css" type="text/css" rel="stylesheet"/>
<style>
    .circle {
        position: relative;
        display: block;
        margin: auto;
        margin-top:10px;
        background-color: transparent;
        color: navajowhite;
        text-align: center;
        width: 50%;
        height: auto;
    }

    .circle:after {
        display: block;
        padding-bottom: 100%;
        width: 100%;
        height: 0;
        border-radius: 50%;
        content: "";
        background-color: #00a1cb;
    }

    .circle__inner {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .circle__wrapper {
        display: table;
        width: 100%;
        height: 100%;
    }

    .circle__content {
        display: table-cell;
        vertical-align: top;
        padding-top: 45%;
        text-align: center;
    }
    .items, .meals {
        color: black !important;
        overflow-x: hidden !important;

    }
    ul li {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
</style>
<form method="POST" >
    <div data-title="Monday" class="monday container" style="background-color: #FFEBD9 !important; padding:10px; border-radius:5px;">
        {!! Form::select('monday', $categories, null, ['class' => 'form-control']) !!}

        <div class="order row " tabindex="1" style="margin:10px;">
            <label class="label label-info col-md-12" style="margin-bottom: 10px;" >Choose meals:</label>
            <ul class="items list-group col-md-6 col-xs-6 col-sm-6">
                @foreach($monday as $meal)
                    <li class="list-group-item zoomInUp animated" id="{{$meal->id}}" price="{{$meal->price}}">{{$meal->name}}<span class="badge">MKD {{$meal->price}}</span></li>
                @endforeach
            </ul>
            <ul class="meals list-group col-md-6 col-xs-6 col-sm-6">
                @foreach($order as $meal)
                    <li class="list-group-item" id="{{$meal->item->id}}" price="{{$meal->item->price}}" class="fc-field-list ui-sortable fc-destination-fields">{{$meal->item->name}} <span class="badge">MKD {{$meal->item->price}}</span></li>
                @endforeach
            </ul>
        </div>
        <div class="row" style="margin: 10px;">
            <label class="label label-info col-md-12" style="margin-top: 30px; margin-bottom: 10px;" >Choose time:</label>

            {!! Form::select('monday-time', $times, null, ['class' => 'form-control col-md-6']) !!}
        </div>

    </div>
    <div class="row col-md-12">
        <button type="submit" class="btn btn-primary pull-right placeOrder">Place order</button>
    </div>

    <input type="hidden" class="monday-mealsIds" name="monday-meals"/>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

</form>
    <script>
        $(document).ready(function(){

            $(".placeOrder").click(function() {
                //monday
                var meals = $(".monday .meals li");
                var mealIds = "";
                meals.each(function () {
                    mealIds += $(this).attr('id') + ",";
                });
                $(".monday-mealsIds").val(mealIds);


            });
                $('select').change(function () {
                var day = $(this).attr('name');
                var category = $(this).val();

                $.ajax({
                    url: '/get-items/' + category + '/' + day,
                    method: 'GET',
                    success: function (data) {
                        if ($("." + day + " .items").length > 0) {
                            $("." + day + " .items li").each(function(){$(this).removeClass('zoomInUp').addClass('animated rotateOut')});
                            var items = $("." + day + " .items li");
                            setTimeout(function(){items.remove();}, 1000);
                        }
                        var i = 0;
                        for (i = 0; i < data.length; i++) {
                            $("." + day + " .items").append($("<li>", {"id": data[i].id, "html": data[i].name + "<span class='badge'>MKD "+data[i].price+"</span>", "price": data[i].price}).addClass('list-group-item zoomInUp animated'));
                        }


                    }
                });

            });

            var container = $(".order");
            container.fieldChooser(container.find(".items"), container.find('.meals'));

            $(".meals").hover(
                    function(){$(".bounceOut").remove()
                        if($(this).children.length > 0){
                            $(this).append("<div class='circle animated bounceIn'> " +
                                    "<div class='circle__inner'> " +
                                    "<div class='circle__wrapper'> " +
                                    "<div class='circle__content'>Drop items here</div> " +
                                    "</div> </div> </div>");}},
                    function(){$(".bounceIn").addClass('bounceOut').removeClass('bounceIn');
                        setTimeout(function(){$(".bounceOut").remove()}, 500);})
        })
    </script>

@endsection