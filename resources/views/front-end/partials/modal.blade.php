<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="color:darkslategray !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myModalLabel">Information!</h4>
            </div>
            <div class="modal-body" style="margin-top:20px; margin-bottom:20px;">
                You have reached the limit of 150 MKD for this order.
                You don't have enough money for ordering this item too!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="yes" style="background-color:#d9534f !important; border-color:#d9534f !important;">Ok</button>
            </div>
        </div>
    </div>
</div>