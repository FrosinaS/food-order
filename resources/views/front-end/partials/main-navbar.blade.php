
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<style>
    .dropdown-menu
    {
        margin-top:0px !important;
    }

    @media (max-width: 767px) {
        .navbar-nav {
            margin-top: 50px !important;
        }
        .navbar-collapse {
            overflow-y: visible !important;
        }
    }
</style>
<nav class="navbar navbar-inverse ">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Food order</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <li><a href="/make-order">Make order</a></li>
                    <li><a href="/my-order">This week order</a></li>
                    <li><a href="/next-week-order">Next week order</a></li>
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account <span class="caret"></span></a>
                    @if(Auth::check())
                        <ul class="dropdown-menu">
                            <li><a href="/password/change">Change password</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/auth/logout">Log out</a></li>
                        </ul>
                    @else
                        <ul class="dropdown-menu">
                            <li><a href="/auth/login">Log in</a></li>
                        </ul>
                    @endif
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
