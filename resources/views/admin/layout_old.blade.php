<!DOCTYPE html>
<html lang="en">
    <head>
        @include('admin.partials.head')
        @include('admin.partials.head_scripts')
    </head>
    <body >
        <div id='overlay'></div>
        <!-- Wrapper -->
        <div id="wrapper">
         
            @include('admin.partials.header')
            
            <!-- Content -->
            <div id="page-wrapper">
            @yield('content')
            </div>
            
        </div>
        <!-- Wrapper END -->
        
        <!-- Footer -->
        @include('admin.partials.footer')
        <!-- Footer END -->
        
        <!-- scripts -->
        @include('admin.partials.scripts')
        <!-- scripts END-->
    </body>
</html>

