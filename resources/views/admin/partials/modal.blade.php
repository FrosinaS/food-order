<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="color:darkslategray !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" style="margin-top:20px; margin-bottom:20px;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="no" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="yes" style="background-color:#d9534f !important; border-color:#d9534f !important;">Yes</button>
            </div>
        </div>
    </div>
</div>