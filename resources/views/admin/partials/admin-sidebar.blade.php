<!-- start: Side Menu -->
<style>
    .sidebar-nav
    {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    #sidebar-left
    {
        background-color: #34495e !important;
    }
    .main-menu ul> li:first-child a
    {
        border-top: 1px solid #666;
    }
</style>
<div id="sidebar-left" class="span2">

    <div class="sidebar-nav collapse navbar-collapse" id="bs-example-navbar-collapse-2">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="{{\URL::to('/admin/users')}}"><i class="glyphicon glyphicon-user"></i><span class="hidden-tablet"> Users</span></a></li>
            <li><a href="{{\URL::to('/admin/times')}}"><i class="glyphicon glyphicon-time"></i><span class="hidden-tablet">  Times</span></a></li>
            <li><a href="{{\URL::to('/admin/categories')}}"><i class="glyphicon glyphicon-menu-hamburger"></i><span class="hidden-tablet"> Categories</span></a></li>
            <li><a href="{{\URL::to('/admin/items')}}"><i class="glyphicon glyphicon-menu-hamburger"></i><span class="hidden-tablet"> Menu</span></a></li>
            <li><a href="{{\URL::to('/admin/orders')}}"><i class="glyphicon glyphicon-shopping-cart"></i><span class="hidden-tablet">  Today orders</span></a></li>
            <li><a href="{{\URL::to('/admin/orders-table')}}"><i class="glyphicon glyphicon-list-alt"></i><span class="hidden-tablet">  Orders table</span></a></li>
            <li><a href="{{\URL::to('/admin/this-week-order')}}"><i class="glyphicon glyphicon-th-list"></i><span class="hidden-tablet">  This week orders</span></a></li>
            <li><a href="{{\URL::to('/admin/reports')}}"><i class="glyphicon glyphicon-calendar"></i><span class="hidden-tablet">  Summary reports</span></a></li>
        </ul>
    </div>
</div>


<!-- end: Side Menu -->

<noscript>
    <div class="alert alert-block span10">
        <h4 class="alert-heading">Warning!</h4>

        <p>You need to have <a src="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
            enabled to use this site.</p>
    </div>
</noscript>

<script>
    $(document).ready(function(){

    });
</script>