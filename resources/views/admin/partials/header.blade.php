<header class="main-header"><!-- header -->
    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="users">Users<span class="sr-only">(current)</span></a></li>
        <li><a href="categories">Categories</a></li>
        <li><a href="items">Items</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Orders <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="orders/pending">Pending</a></li>
            <li><a href="orders/report">Report</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        
        <li>
           <a href="/auth/logout">LogOut</a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


</header>