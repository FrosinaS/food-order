@extends('admin.admin-main-layout')
@section('main-content')
    <style>
        .status {
            width: 120px;
        }

        table tr th {
            text-align: center;
        }

        table tr td {
            width: auto;
            padding: 15px !important;
            text-align: center !important;
        }
        .label-success
        {
            background-color:#1abc9c !important;
        }
        .checkbox
        {
            margin-left:50px !important;
        }

        table th:first-child {
            width: 181px !important;
        }
        .list-group-item {
            display: inline-block !important;
            width: auto;
            margin-right: 10px;
            margin-bottom:10px;
        }

        #Container .mix
        {
            display:none;
        }

        .pager-list  .active
        {
            background-color:lightgray;
            color:gray;
            font-weight:bold;
        }
        .well div:first-child > button
        {
            margin-right: 3px;
            margin-top:10px;
        }

        .pager
        {
            background-color:#1abc9c !important;
        }

    </style>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>Manage items</li>
    </ul>


    <div class="row" style="margin-bottom:10px !important;">
        <ul class="pull-right" >
            Sort:
            <li class="list-group-item sort" data-sort="my-order:desc">Recent</li>
            <li class="list-group-item sort" data-sort="my-order:asc">Older</li>

        </ul>
        <br/>
        <ul class="pull-right" >
            Filter:
            <li class="list-group-item filter" data-filter="all">All</li>
            <li class="list-group-item filter" data-filter=".monday">Monday</li>
            <li class="list-group-item filter" data-filter=".tuesday">Tuesday</li>
            <li class="list-group-item filter" data-filter=".wednesday">Wednesday</li>
            <li class="list-group-item filter" data-filter=".thursday">Thursday</li>
            <li class="list-group-item filter" data-filter=".friday">Friday</li>

            <li class="list-group-item filter" data-filter=".active">Active</li>
            <li class="list-group-item filter" data-filter=".not-active">Not active</li>



            </ul>
    </div>
    <div class="well">
        <div class="row">
            <div class="col-md-8">
                <button class="btn btn-default addNewItem"><span class="glyphicon glyphicon-plus"></span> New
                </button>
                <button class="btn btn-primary activateItems"><span class="glyphicon glyphicon-ok"></span> Activate
                </button>
                <button class="btn btn-warning deactivateItems"><span class="glyphicon glyphicon-remove"></span>
                    Deactivate
                </button>
                <button class="btn btn-danger deleteItems"><span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" id="search-query-3">
                          <span class="input-group-btn">
                            <button type="submit" class="btn search"><span class="fui-search"></span></button>
                          </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table  table-striped ">
            <thead>
            <tr>
                <th>
                    <button type="button" class="btn btn-link selectAll"><span class="glyphicon glyphicon-ok"></span>
                        <span class="text">Select all</span>
                    </button>
                </th>
                <th>
                    ID
                </th>
                <th>
                    Name
                </th>
                <th>
                    Category
                </th>
                <th>
                    Status
                </th>
                <th>
                    Monday
                </th>
                <th>
                    Tuesday
                </th>
                <th>
                    Wednesday
                </th>
                <th>
                    Thursday
                </th>
                <th>
                    Friday
                </th>
                <th>
                    Vegetarian
                </th>
                <th>
                    Actions
                </th>
            </tr>
            </thead>
            <tbody id="Container">
            @foreach($items as $item)
                <tr class="mix {{$item->status == 1 ? 'active' : 'not-active'}}
                               {{$item->monday == 1 ? 'monday': ""}}
                               {{$item->tuesday == 1 ? 'tuesday' : ''}}
                               {{$item->wednesday == 1 ? 'wednesday' : ''}}
                               {{$item->thursday == 1 ? 'thursday' : ''}}
                               {{$item->friday == 1 ? 'friday' : ''}}" data-my-order="{{$item->id}}">
                    <td>
                        <label class="checkbox" >
                            <input type="checkbox" class="checkItem" data-toggle="checkbox"  id="{{$item->id}}">
                        </label>
                    </td>
                    <td>
                        {{$item->id}}
                    </td>
                    <td>
                        {{$item->name}}
                    </td>
                    <td>
                        {{$item->category->name}}
                    </td>
                    <td class="status">
                        @if($item->status == 1)
                            <div class="label label-success">
                                Active
                            </div>
                        @else
                            <div class="label label-danger ">
                                Not active
                            </div>
                        @endif
                    </td>
                    <td>
                        @if($item->monday == 1)
                            <span class="glyphicon glyphicon-ok" style="color:green;"></span>
                        @else
                            <span class="glyphicon glyphicon-remove" style="color:red;"></span>
                        @endif
                    </td>

                    <td>
                        @if($item->tuesday == 1)
                            <span class="glyphicon glyphicon-ok" style="color:green;"></span>
                        @else
                            <span class="glyphicon glyphicon-remove" style="color:red;"></span>
                        @endif
                    </td>
                    <td>
                        @if($item->wednesday == 1)
                            <span class="glyphicon glyphicon-ok" style="color:green;"></span>
                        @else
                            <span class="glyphicon glyphicon-remove" style="color:red;"></span>
                        @endif
                    </td>
                    <td>
                        @if($item->thursday == 1)
                            <span class="glyphicon glyphicon-ok" style="color:green;"></span>
                        @else
                            <span class="glyphicon glyphicon-remove" style="color:red;"></span>
                        @endif
                    </td>
                    <td>
                        @if($item->friday == 1)
                            <span class="glyphicon glyphicon-ok" style="color:green;"></span>
                        @else
                            <span class="glyphicon glyphicon-remove" style="color:red;"></span>
                        @endif
                    </td>

                    <td>
                        @if($item->vegetarian == 1)
                            <span class="glyphicon glyphicon-ok" style="color:green;"></span>
                        @else
                            <span class="glyphicon glyphicon-remove" style="color:red;"></span>
                        @endif
                    </td>
                    <td>
                        <a href="/admin/items/edit-item/{{$item->id}}">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>


        </table>

        <div class="pager-list pull-right">

        </div>
    </div>

    <script>
        jQuery(document).ready(function ($) {

            $("input[type='checkbox']").radiocheck();


            $(".addNewItem").click(function () {
                window.location.replace("/admin/items/add-new");
            });

            $(".activateItems").click(function () {
                var items = $(":checkbox:checked");
                if(items.length) {
                    var itemIds = [];
                    items.each(function () {
                        itemIds.push($(this).attr('id'));
                    });
                    $.ajax({
                        url: '/admin/items/activate',
                        method: 'POST',
                        data: {
                            itemIds: itemIds,
                            _token: '{{ csrf_token()}}'
                        },
                        success: function () {
                            items.each(function () {
                                $(this).parent().parent().parent().find('.status').html("<div class='label label-success'>Active</div>");
                                $(this).parent().parent().parent().removeClass('not-active').addClass('active');
                                $(this).prop('checked', false);
                                $(".text").html("Select all");
                            })
                        }
                    });
                }
            });

            $(".deactivateItems").click(function () {
                var items = $(":checkbox:checked");
                if(items.length > 0) {
                    var itemsIds = [];
                    items.each(function () {
                        itemsIds.push($(this).attr('id'));
                    });
                    $.ajax({
                        url: '/admin/items/deactivate',
                        method: 'POST',
                        data: {
                            itemIds: itemsIds,
                            _token: '{{ csrf_token()}}'
                        },
                        success: function () {
                            items.each(function () {
                                $(this).parent().parent().parent().find('.status').html("<div class='label label-danger'>Not active</div>");
                                $(this).parent().parent().parent().removeClass('active').addClass('not-active');
                                $(this).prop('checked', false);
                                $(".text").html("Select all");
                            })
                        }
                    });
                }
            });

            $(".deleteItems").click(function () {
                if($(":checkbox:checked").length > 0) {
                    $(".modal-title").html("Delete items");
                    $(".modal-body").html("Are you sure you want to delete these items?");
                    $("#basicModal").modal();
                }
            });

            $("#no").click(function () {
                $("#basicModal").modal('hide');
            });

            $("#yes").click(function () {
                var items = $(":checkbox:checked");
                var itemsIds = [];
                items.each(function () {
                    itemsIds.push($(this).attr('id'));
                });
                $.ajax({
                    url: '/admin/items/delete',
                    method: 'POST',
                    data: {
                        itemIds: itemsIds,
                        _token: '{{ csrf_token()}}'
                    },
                    success: function () {
                        $("#basicModal").modal('hide');
                        items.each(function () {
                            $(this).parent().parent().parent().remove()
                        });

                    }
                });
            });

            $(".selectAll").click(function () {
                if ($(".text").html() == "Select all") {
                    $(".checkItem").prop("checked", true);
                    $(".text").html("Unselect all");
                }
                else {
                    $(".checkItem").prop("checked", false);
                    $(".text").html("Select all");
                }
            });

            $(".search").click(function () {
                window.location.replace('/admin/items/search/' + $("#search-query-3").val());
            });

            $('#Container').mixItUp({
                load: {
                    filter: 'all',
                    sort: 'my-order:desc'
                },
                controls: {
                    toggleFilterButtons: true,
                    toggleLogic: 'and'
                },
                layout: {
                    display: 'table-row'
                },
                pagination: {
                    limit:5,
                    generatePagers:true,
                    pagerClass: 'btn',
                    prevButtonHTML: 'Previous',
                    nextButtonHTML: 'Next',
                    loop: false,
                    load: 1
                }
            });

        });
    </script>

    @include("admin.partials.modal")
@endsection


