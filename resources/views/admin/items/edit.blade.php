@extends('admin.admin-main-layout')

@section('main-content')
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <a class="btn btn-default navbar-btn" href="{{\URL::to('/admin/items')}}">Back</a>
    </div><!-- /.container-fluid -->
</nav>


{!! Form::open(array('url'=>'/admin/items/'.$item->id, 'method' => 'put', 'id' => 'items-edit-form')) !!}

<!--   {!! Form::token() !!}-->
<div class="form-group">
    {!! Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')) !!}
    {!!  Form::text('name', $item->name,  array('class' => 'form-control')) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) !!}
    {!!  Form::text('description', $item->description, array('class' => 'form-control')) !!}
</div>

<div class="form-group">
    {!! Form::label('category', 'Category', array('class' => 'col-sm-2 control-label')) !!}
    <?php $categories_array = array(); ?>
    <?php
    foreach ($categories as $category) {
        $categories_array[$category->id] = $category->category_name;
    }
    ?>
    {!! Form::select('category', $categories_array, $item->category_id ); !!}
</div>

<div class="form-group">
    {!! Form::label('price', 'Price', array('class' => 'col-sm-2 control-label')) !!}
    {!!  Form::text('price', $item->price , array('class' => 'form-control')) !!}
</div>

<div class="form-group">
    {!! Form::label('weight', 'Weight', array('class' => 'col-sm-2 control-label')) !!}
    {!!  Form::text('weight', $item->weight , array('class' => 'form-control')) !!}
</div>

<?php
if ($item->status == '1') {
    $active = true;
    $not_active = false;
} else {
    $active = false;
    $not_active = true;
}
?>
<div class="form-group">
    {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
    {!! Form::radio('status', '1', $active);!!} Active
    {!! Form::radio('status', '0', $not_active);!!} Not Active
</div>
<div class="form-group">

    {!! Form::label('days', 'Select days', array('class' => 'col-sm-2 control-label')) !!}
    <?php $mon = ($item->monday == '1') ? true : false; ?>
    <?php $tues = ($item->tuesday == '1') ? true : false; ?>
    <?php $wed = ($item->wednesday == '1') ? true : false; ?>
    <?php $thurs = ($item->thursday == '1') ? true : false; ?>
    <?php $fri = ($item->friday == '1') ? true : false; ?>

    {!! Form::checkbox('monday', '1', $mon) !!} Monday
    {!! Form::checkbox('tuesday', '1', $tues) !!} Tuesday
    {!! Form::checkbox('wednesday', '1', $wed) !!} Wednesday
    {!! Form::checkbox('thursday', '1', $thurs) !!} Thursday
    {!! Form::checkbox('friday', '1', $fri) !!} Friday
</div>

<div class="col-sm-6">
    <button type="submit" class="btn btn-default">Submit</button>
</div>
</div>
<div class="form-group">

</div>
{!! Form::close() !!}

@endsection
