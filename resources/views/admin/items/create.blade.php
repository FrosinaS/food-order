@extends('admin.admin-main-layout')

@section('main-content')
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <a class="btn btn-default navbar-btn" href="{{\URL::to('/admin/items')}}">Back</a>
    </div><!-- /.container-fluid -->
</nav>


{!! Form::open(array('url'=>'/admin/items', 'method' => 'post', 'id' => 'items-create-form')) !!}

<!--   {!! Form::token() !!}-->
<div class="form-group">
    {!! Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')); !!}
    {!!  Form::text('name', '',  array('class' => 'form-control')); !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')); !!}
    {!!  Form::text('description', '', array('class' => 'form-control')); !!}
</div>

<div class="form-group">
    {!! Form::label('category', 'Category', array('class' => 'col-sm-2 control-label')) !!}
    <?php $categories_array = array(); ?>
    <?php
    foreach ($categories as $category) {
        $categories_array[$category->id] = $category->category_name;
    }
    ?>
    {!! Form::select('category', $categories_array ); !!}
</div>

<div class="form-group">
    {!! Form::label('price', 'Price', array('class' => 'col-sm-2 control-label')); !!}
    {!!  Form::text('price', '', array('class' => 'form-control')); !!}
</div>

<div class="form-group">
    {!! Form::label('weight', 'Weight', array('class' => 'col-sm-2 control-label')); !!}
    {!! Form::text('weight', '', array('class' => 'form-control')); !!}
</div>

<div class="form-group">
    {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
    {!! Form::radio('status', '1');!!} Active
    {!! Form::radio('status', '0');!!} Not Active
</div>
<div class="form-group">
    {!! Form::label('days', 'Select days', array('class' => 'col-sm-2 control-label')); !!}
    {!! Form::checkbox('monday', '1'); !!} Monday
    {!! Form::checkbox('tuesday', '1'); !!} Tuesday
    {!! Form::checkbox('wednesday', '1'); !!} Wednesday
    {!! Form::checkbox('thursday', '1'); !!} Thursday
    {!! Form::checkbox('friday', '1'); !!} Friday
</div>


<button type="submit" class="btn btn-default">Submit</button>
</div>
<div class="form-group">

</div>
{!! Form::close() !!}

@endsection
