@extends('admin.admin-main-layout')
@section('main-content')
    <style>
        .checkbox.inline
        {
            white-space: nowrap;
        }
    </style>
    <div class="container-fluid" style="text-align:center;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="background-color:#1abc9c;">
                    <h3>Food Order </h3>

                    <div class="panel-heading h4"
                         style="background-color:#1abc9c; border-top:2px solid slategray; border-bottom: none;">
                        {!! $header !!}

                    </div>
                    <div class="panel-body" style="background-color:white; border-top:2px solid slategray;">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <form class="form-horizontal" role="form" method="POST" style="margin-top:30px;">


                            <input type="hidden" name="_token" value="{{ csrf_token() }}">




                            <div class="form-group row">
                                <label class="col-md-4 control-label">Name: </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name"
                                           value="{{$item->name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label">Description: </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="description"
                                           value="{{$item->description}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 control-label">Price: </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="price"
                                           value="{{$item->price}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label">Weight: </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="weight"
                                           value="{{$item->weight}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-sm-3 col-xs-3 control-label">Monday: </label>

                                <div class="col-md-6 col-sm-3 col-xs-3" style="margin-top:10px;">
                                    <label class="checkbox" style="margin-left: 40px;">
                                        <input type="checkbox"  data-toggle="checkbox" name="monday">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-sm-3 col-xs-3 control-label">Tuesday: </label>

                                <div class="col-md-6 col-sm-3 col-xs-3" style="margin-top:10px;">
                                    <label class="checkbox" style="margin-left: 40px;">
                                        <input type="checkbox"  data-toggle="checkbox" name="tuesday">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-sm-3 col-xs-3 control-label">Wednesday: </label>

                                <div class="col-md-6 col-sm-3 col-xs-3" style="margin-top:10px;">
                                    <label class="checkbox" style="margin-left: 40px;">
                                        <input type="checkbox"  data-toggle="checkbox" name="wednesday">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-sm-3 col-xs-3 control-label">Thursday: </label>

                                <div class="col-md-6 col-sm-3 col-xs-3" style="margin-top:10px;">
                                    <label class="checkbox" style="margin-left: 40px;">
                                        <input type="checkbox"  data-toggle="checkbox" name="thursday">
                                    </label>
                                </div>
                            </div><div class="form-group row">
                                <label class="col-md-4 col-sm-3 col-xs-3 control-label">Friday: </label>

                                <div class="col-md-6 col-sm-3 col-xs-3" style="margin-top:10px;">
                                    <label class="checkbox" style="margin-left: 40px;">
                                        <input type="checkbox"  data-toggle="checkbox" name="friday">
                                    </label>
                                </div>
                            </div>




                            <div class="form-group row">
                                <label class="col-md-4 control-label">Category: </label>

                                <div class="col-md-6">
                                    <select name="category" class="form-control">
                                       @foreach($categories as $category)
                                             <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label">Status: </label>

                                <div class="col-md-6">
                                    <select name="status" class="form-control">
                                        <option value="active">Active</option>
                                        <option value="not-active">Not active</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-sm-3 col-xs-3 control-label">Vegetarian: </label>

                                <div class="col-md-6 col-sm-3 col-xs-3" style="margin-top:10px;">
                                    <label class="checkbox" style="margin-left: 40px;">
                                        <input type="checkbox"  data-toggle="checkbox" name="vegetarian">
                                    </label>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary pull-right">
                                        {!! $btnText !!}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
</div>
<script>

    $(document).ready(function(){
        $(":checkbox").each(function() {
            $(this).radiocheck();
        });


        $('input[name^="vegetarian"]').prop("checked", "{{$item->vegetarian == 1 ? true : false}}");
        $('input[name^="monday"]').prop("checked", "{{$item->monday == 1 ? true : false}}");
        $('input[name^="tuesday"]').prop("checked", "{{$item->tuesday == 1 ? true : false}}");
        $('input[name^="wednesday"]').prop("checked", "{{$item->wednesday == 1 ? true : false}}");
        $('input[name^="thursday"]').prop("checked", "{{$item->thursday == 1 ? true : false}}");
        $('input[name^="friday"]').prop("checked", "{{$item->friday == 1 ? true : false}}");

        $('select[name^="status"] option:selected').attr("selected", null);
        $('select[name^="status"] option[value="{{$item->status == 1 ? 'active' : 'not-active'}}"]').attr("selected", "selected");

        $('select[name^="category"] option:selected').attr("selected", null);
        $('select[name^="category"] option[value="{{$item->category->id}}"]').attr("selected", "selected");

    })
    </script>

@endsection
