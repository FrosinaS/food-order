@extends('admin.admin-main-layout')

@section('main-content')

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <a class="btn btn-default navbar-btn" href="items/create">New</a>
        <a class="btn btn-default navbar-btn" id='activate_btn'>Activate</a>
        <a class="btn btn-default navbar-btn" id='deactivate_btn'>Deactivate</a>
        <button class="delete_btn btn btn-default navbar-btn" id="delete_btn" >Delete</button>
        <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
<!--        <button type="button" class="btn btn-default navbar-btn">Reset</button>-->

    </div><!-- /.container-fluid -->
</nav>


<h3>Filter menu items by:</h3>
<div>
    {!! Form::label('category', 'Category', array('class' => '')) !!}
    <?php $categories_array=array();?>
    <?php foreach ($categories as $category){ ?>
        <a>{!! $category->category_name !!}</a>
    <?php } ?>

</div>

<div>
    {!! Form::label('status', 'Status', array('class' => '')) !!}
    <a>Active</a>
    <a>Deactive</a>
</div>



<form id="read_items_form" action="" data-url='{{\URL::to('/admin/items')}}' method="post">
    {!! Form::token() !!}

    <table  class="table table-striped">
        <thead>
            <tr style="font-weight: bold;" style="padding: 10px;">

                <th><input type="checkbox" id="select_all" name="select_all" value="0"></th>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Category</th>
                <th>Weight</th>
                <th>Price</th>
                <th>Status</th>
                <th>Created at</th>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($items)) { 
                foreach ($items as $item): ?>

                <tr>
                    <th><input type="checkbox" name="{!! $item->id; !!}" value="{!! $item->id; !!}" class="check"></th>
                    <td>{!! $item->id; !!}</td>
                    <td>{!! $item->name; !!}</td>
                    <td>{!! $item->description; !!}</td>
                    <td><?php echo ($item->monday== 1) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "";?></td>
                    <td><?php echo ($item->tuesday== 1) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "";?></td>
                    <td><?php echo ($item->wednesday== 1) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "";?></td>
                    <td><?php echo ($item->thursday== 1) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "";?></td>
                    <td><?php echo ($item->friday== 1) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : "";?></td>
                    <td>{!! $item->category_name; !!}</td>
                    <td>{!! $item->weight; !!}</td>
                    <td>{!! $item->price; !!}</td>
                    <td><?php echo ($item->status == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-important">Not active</span>';?></td>
                    <td>{!! $item->created_at; !!}</td>
                    <td><a href="items/{!! $item->id;!!}/edit">Edit</a></td>   
                </tr>

            <?php endforeach; } ?>
              
        </tbody>
    </table> 

</form>
<div class="page-header">
    Filter items by:
</div>
<ul class="nav nav-list fiter_users">
    <?php if(!empty($categories)) { ?>
    <li class="nav-header">Categories</li>
    <?php foreach($categories as $category) { ?>
    <li <?php if (Input::get('category') == $category->id) { ?> class="active" <?php } ?>>
        <a href="javascript://" class="category" data-category="<?php echo $category->id; ?>"><?php echo $category->category_name; ?></a>
        <?php if (Input::get('category') == $category->id) { ?> 
            <a href="javascript://" class="data_remove remove_category" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>
    <?php } } ?>
    
    <li class="nav-header">Price</li>
    <li <?php if (Input::get('price') == 'asc') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="price" data-price="asc">Low - High</a>
        <?php if (Input::get('price') == 'asc') { ?> 
            <a href="javascript://" class="data_remove remove_price" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>
    <li <?php if (Input::get('price') == 'desc') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="price" data-price="desc">High - Low</a>
        <?php if (Input::get('price') == 'desc') { ?> 
            <a href="javascript://" class="data_remove remove_price" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>
    
    <li class="nav-header">Date created</li>
    <li <?php if (Input::get('created') == 'recent') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="created" data-created="recent">Recent</a>
        <?php if (Input::get('created') == 'recent') { ?> 
            <a href="javascript://" class="data_remove remove_created" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>
    <li <?php if (Input::get('created') == 'old') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="created" data-created="old">Older</a>
        <?php if (Input::get('created') == 'old') { ?> 
            <a href="javascript://" class="data_remove remove_created" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>

    <li class="nav-header">Status</li>
    <li <?php if (Input::get('status') == 'active') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="status" data-status="active">Active</a>
        <?php if (Input::get('status') == 'active') { ?> 
            <a href="javascript://" class="data_remove remove_status" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>
    <li <?php if (Input::get('status') == 'not_active') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="status" data-status="not_active">Not active</a>
        <?php if (Input::get('status') == 'not_active') { ?> 
            <a href="javascript://" class="data_remove remove_status" title="Remove filter"><i class="icon-remove"></i></a>
        <?php } ?>
    </li>
</ul>

<input type="hidden" id="category" value="<?php echo Input::get('category') ?>" />
<input type="hidden" id="price" value="<?php echo Input::get('price') ?>" />
<input type="hidden" id="created" value="<?php echo Input::get('created') ?>" />
<input type="hidden" id="status" value="<?php echo Input::get('status') ?>" />
@endsection


<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        
        $("#select_all").on('click', function() {

        if(this.checked) {
            // Iterate each checkbox
            $('.check').each(function() {
                  this.checked = true;

            });
        }
        else {
          $('.check').each(function() {
                this.checked = false;
            });
        }
        
        
    });
    
    $('#delete_btn').on('click', function() {
        var url = $("#read_items_form").attr('data-url') + '/deleteItems';
        
        $("#read_items_form").attr('action',url); 
        $("#read_items_form").submit();
    });
    
    $('#activate_btn').on('click', function() {
        var url = $("#read_items_form").attr('data-url') + '/activateItems';
        
        $("#read_items_form").attr('action',url); 
        $("#read_items_form").submit();
    });
    
     $('#deactivate_btn').on('click', function() {
        var url = $("#read_items_form").attr('data-url') + '/deactivateItems';
        
        $("#read_items_form").attr('action',url); 
        $("#read_items_form").submit();
    });
    
    
    
    function search_query() {

        var category = $("#category").val();
        var status = $("#status").val();
        var price = $("#price").val();
        var created = $("#created").val();
        var query = [];
        
        if (category.length > 0) {
            query.push('category=' + category);
        }
        if (status.length > 0) {
            query.push('status=' + status);
            
        }
        if (price.length > 0) {
            query.push('price=' + price);
        }
        if (created.length > 0) {
            query.push('created=' + created);
        }

        var query_url = 'admin/items?' + query.join('&');
        var url= window.location.protocol + "//" + window.location.host + "/"+ query_url;
        location.href = url;
    
    }
    
    $('.fiter_users').find('a.category').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#category').val($(this).attr('data-category'));
        search_query();
    });

    $('.fiter_users').find('a.remove_category').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#category').val('');
        search_query();
    });
    
    $('.fiter_users').find('a.price').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#price').val($(this).attr('data-price'));
        search_query();
    });

    $('.fiter_users').find('a.remove_price').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#price').val('');
        search_query();
    });

    $('.fiter_users').find('a.created').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#created').val($(this).attr('data-created'));
        search_query();
    });

    $('.fiter_users').find('a.remove_created').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#created').val('');
        search_query();
    });

    $('.fiter_users').find('a.status').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#status').val($(this).attr('data-status'));
        search_query();
    });

    $('.fiter_users').find('a.remove_status').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('#status').val('');
        search_query();
    });

    
    })


</script>

</head>

 