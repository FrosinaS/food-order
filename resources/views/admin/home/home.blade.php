@extends('admin.layout')

@section('content')

<!-- Sidebar navigation i welcome to admin msg -->
<ul class="nav nav-pills nav-stacked" style="width: 15%; float: left;">
  <li role="presentation" class="active">
      <a href="users">Users</a>
  </li>
  <li role="presentation">
      <a href="categories">Categories</a>
  </li>
  <li role="presentation">
      <a href="items">Items</a>
  </li>
  <li role="presentation">
      <a href="orders/pending">Pending users</a>
  </li>
  <li role="presentation">
      <a href="orders/report">Report</a>
  </li>
</ul>

<div class="jumbotron"  style="width: 50%; margin-left: 20%;">
  <h3>Admin Dashboard</h3>
  <p>...</p>
</div>
@endsection