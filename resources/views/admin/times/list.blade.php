@extends('admin.admin-main-layout')
@section('main-content')
    <style>
        .status {
            width: 120px;
        }

        table tr th {
            text-align: center;
        }

        table tr td {
            width: auto;
            padding: 15px !important;
            text-align: center !important;
        }
        .label-success
        {
            background-color:#1abc9c !important;
        }
        .checkbox
        {
            margin-left:50px !important;
        }

        table th:first-child {
            width: 181px !important;
        }
        .list-group-item {
            display: inline-block !important;
            width: auto;
            margin-right: 10px;
            margin-bottom:10px;
        }

        #Container .mix
        {
            display:none;
        }

        .pager-list  .active
        {
            background-color:lightgray;
            color:gray;
            font-weight:bold;
        }
        .well div:first-child > button
        {
            margin-right: 3px;
            margin-top:10px;
        }

        .pager
        {
            background-color:#1abc9c !important;
        }

    </style>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>Manage times</li>
    </ul>


    <div class="row" style="margin-bottom:10px !important;">
        <ul class="pull-right" >
            Sort:
            <li class="list-group-item sort" data-sort="my-order:desc">Recent</li>
            <li class="list-group-item sort" data-sort="my-order:asc">Older</li>
            Filter:
            <li class="list-group-item filter" data-filter="all">All</li>
            <li class="list-group-item filter activeUser" data-filter=".active">Active</li>
            <li class="list-group-item filter notActiveUser" data-filter=".not-active">Not active</li>
        </ul>
    </div>
    <div class="well">
        <div class="row">
            <div class="col-md-8">
                <button class="btn btn-default addNewTime"><span class="glyphicon glyphicon-plus"></span> New
                </button>
                <button class="btn btn-primary activateTime"><span class="glyphicon glyphicon-ok"></span> Activate
                </button>
                <button class="btn btn-warning deactivateTime"><span class="glyphicon glyphicon-remove"></span>
                    Deactivate
                </button>
                <button class="btn btn-danger deleteTime"><span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table  table-striped ">
            <thead>
            <tr>
                <th>
                    <button type="button" class="btn btn-link selectAll"><span class="glyphicon glyphicon-ok"></span>
                        <span class="text">Select all</span>
                    </button>
                </th>
                <th>
                    ID
                </th>
                <th>
                    Time
                </th>
                <th>
                  Max people
                </th>
                <th>
                    Status
                </th>
                <th>
                    Created at
                </th>
                <th>
                    Actions
                </th>
            </tr>
            </thead>
            <tbody id="Container">
            @foreach($times as $time)
                <tr class="mix {{$time->status == 1 ? 'active' : 'not-active'}}" data-my-order="{{$time->id}}">
                    <td>
                        <label class="checkbox" >
                            <input type="checkbox" class="checkTime" data-toggle="checkbox"  id="{{$time->id}}">
                        </label>
                    </td>
                    <td>
                        {{$time->id}}
                    </td>
                    <td>
                        {{$time->time}}
                    </td>
                    <td>
                        {{$time->max_people}}
                    </td>
                    <td class="status">
                        @if($time->status == 1)
                            <div class="label label-success">
                                Active
                            </div>
                        @else
                            <div class="label label-danger ">
                                Not active
                            </div>
                        @endif
                    </td>
                    <td>
                        {{date('D, d M Y, H:i', strtotime($time->created_at))}}
                    </td>
                    <td>
                        <a href='/admin/times/edit-time/{{$time->id}}' style="color:#1abc9c">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>


        </table>
    </div>

    <script>
        $(document).ready(function () {

            $("input[type='checkbox']").radiocheck();


            $(".addNewTime").click(function () {
                window.location.replace("/admin/times/add-new-time");
            });

            $(".activateTime").click(function () {
                var times = $(":checkbox:checked");
                if(times.length > 0) {
                    var timesIds = [];
                    times.each(function () {
                        timesIds.push($(this).attr('id'));
                    });
                    $.ajax({
                        url: '/admin/times/activate',
                        method: 'POST',
                        data: {
                            timeIds: timesIds,
                            _token: '{{ csrf_token()}}'
                        },
                        success: function () {
                            times.each(function () {
                                $(this).parent().parent().parent().find('.status').html("<div class='label label-success'>Active</div>");
                                $(this).parent().parent().parent().removeClass('not-active').addClass('active');
                                $(this).prop('checked', false);
                                $(".text").html("Select all");
                            })
                        }
                    });
                }
            });

            $(".deactivateTime").click(function () {
                var times = $(":checkbox:checked");
                if(times.length > 0){
                var timesIds = [];
                times.each(function () {
                    timesIds.push($(this).attr('id'));
                });
                $.ajax({
                    url: '/admin/times/deactivate',
                    method: 'POST',
                    data: {
                        timeIds: timesIds,
                        _token: '{{ csrf_token()}}'
                    },
                    success: function () {
                        times.each(function () {
                            $(this).parent().parent().parent().find('.status').html("<div class='label label-danger'>Not active</div>");
                            $(this).parent().parent().parent().removeClass('active').addClass('not-active');
                            $(this).prop('checked', false);
                            $(".text").html("Select all");
                        })
                    }
                });
                    }
            });

            $(".deleteTime").click(function () {
                if($(":checkbox:checked").length > 0) {
                    $(".modal-title").html("Delete times");
                    $(".modal-body").html("Are you sure you want to delete these times?");
                    $("#basicModal").modal();
                }
            });

            $("#no").click(function () {
                $("#basicModal").modal('hide');
            });

            $("#yes").click(function () {
                var times = $(":checkbox:checked");
                var timesIds = [];
                times.each(function () {
                    timesIds.push($(this).attr('id'));
                });
                $.ajax({
                    url: '/admin/times/delete',
                    method: 'POST',
                    data: {
                        timeIds: timesIds,
                        _token: '{{ csrf_token()}}'
                    },
                    success: function () {
                        $("#basicModal").modal('hide');
                        times.each(function () {
                            $(this).parent().parent().parent().remove()
                        });

                    }
                });
            });

            $(".selectAll").click(function () {
                if ($(".text").html() == "Select all") {
                    $(".checkTime").prop("checked", true);
                    $(".text").html("Unselect all");
                }
                else {
                    $(".checkTime").prop("checked", false);
                    $(".text").html("Select all");
                }
            });

            $('#Container').mixItUp({
                load: {
                    filter: 'all',
                    sort: 'my-order:desc'
                },
                controls: {
                    toggleFilterButtons: true,
                    toggleLogic: 'and'
                },
                layout: {
                    display: 'table-row'
                }
            });

        });
    </script>

    @include("admin.partials.modal")
@endsection


