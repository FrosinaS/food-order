@extends('admin.admin-main-layout')
@section('main-content')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>

<style>
    .ranges ul li
    {
        width:auto !important;
        height:auto !important;
        overflow:hidden !important;
        margin:6px !important;
    }

    .dropdown-menu:after, .dropdown-menu:before
    {
        border-bottom-color: transparent !important;
        left:auto !important;
    }

    .dropdown-menu
    {
    border: 1px solid lightgrey;
    }
</style>
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>Summary reports</li>
    </ul>
    <div class="container well">
        <div id="reportrange" class="pull-left" style="width:auto !important; margin:auto !important;background: #fff; border-radius: 5px; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;color:darkgray;">
            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
            <span></span> <b class="caret"></b>
        </div>
        <button class="btn btn-success getReports" style="margin-left: 30px;">Get report</button>
        <div class="cont  alert alert-success text-primary " style="width: auto !important; margin:50px;">
            <span class="h4">The total price of orders for this period is
            <span class="sum"></span> MKD.</span>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {

            var startDate, endDate;

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

                startDate=start.format('YYYY-M-D');
                endDate=end.format('YYYY-M-D');
            }
            cb(moment().startOf('week').add(1, 'day'), moment().endOf('week').add(1, 'day'), 'This week');

            $('#reportrange').daterangepicker({
                'ranges': {
                    'This week': [moment().startOf('week').add(1, 'day'), moment().endOf('week')]
                }
            }, cb);

            $('.cont').hide();
            $('.getReports').click(function(){

                $.ajax({
                    url: '/admin/get-reports/' + startDate +'/' + endDate,
                    method: 'GET',
                    success: function (data) {
                        if(data > 0) {
                            $('.sum').html(data);
                        }
                        else{
                            $('.sum').html('0');
                        }
                        $('.cont').show();

                    }
                });

            })
        });
    </script>


@endsection




