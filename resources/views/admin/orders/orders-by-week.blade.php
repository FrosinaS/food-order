@extends('admin.admin-main-layout')
@section('main-content')
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>This week orders table</li>
    </ul>


    <style>
        table, th, td {
            border: 1px solid black;
        }
        td, th{

            text-align:center;
            vertical-align: middle !important;
        }
        table {
            border-collapse: collapse;
        }
    </style>
    <h1 style="text-align: center;">Orders by user for
    </h1>
    <?php     $nextweek = strtotime('this week');
    $dates = array(
            date('Y-m-d', strtotime('monday', $nextweek)),
            date('Y-m-d', strtotime('tuesday', $nextweek)),
            date('Y-m-d', strtotime('wednesday', $nextweek)),
            date('Y-m-d', strtotime('thursday', $nextweek)),
            date('Y-m-d', strtotime('friday', $nextweek))
    );
    ?>
    <h1 style="text-align: center;"><b>{{date('D, d M ', strtotime($dates[0]))}}</b> -
        <b>{{date('D, d M ', strtotime($dates[4]))}}</b></h1>


    <div class="table-responsive" style="margin-top:30px;">
        <table class="table table-bordered ">
            <thead>
            <tr>
                <th>User</th>

                @foreach($dates as $date)

                    <th colspan="3">
                        {{date('D, d M', strtotime($date))}}
                    </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <?php
                for($i = 0; $i < 5; $i++){
                ?>
                    <td colspan="2">Meal</td>
                    <td>Time</td>
                <?php
                }
                ?>
            </tr>
            @foreach($orders as $name => $date)
                <tr>
                    <td>
                        {{$name}}
                    </td>
                    @foreach($dates as $datum)
                        @if(array_key_exists($datum, $date))
                            <td colspan="2">
                                <?php $brojac=0; ?>
                            @foreach($orders[$name][$datum][key($orders[$name][$datum])] as $meal)
                                <?php $brojac++; ?>
                                    {{$brojac}}. {{$meal}}<br/>
                            @endforeach
                            </td>
                            <td>{{date('H:i', strtotime(key($orders[$name][$datum])))}}</td>
                        @else
                            <td colspan="2">
                             /
                                </td>
                                <td>/</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
            </tbody>

        </table>
    </div>

@endsection




