@extends('admin.admin-main-layout')
@section('main-content')
    <style>

        table tr th {
            text-align: center;
        }

        table tr td {
            width: 33%;
            padding: 15px !important;
            text-align: center !important;
        }

        .label-success {
            background-color: #1abc9c !important;
        }

        .checkbox {
            margin-left: 50px !important;
        }

        table th:first-child {
            width: 181px !important;
        }

        .list-group-item {
            display: inline-block !important;
            width: auto;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        #Container .mix {
            display: none;
        }

        .pager-list .active {
            background-color: lightgray;
            color: gray;
            font-weight: bold;
        }

        .well div:first-child > button {
            margin-right: 3px;
            margin-top: 10px;
        }

        .pager {
            background-color: #1abc9c !important;
        }

    </style>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>Orders list</li>
    </ul>


    <div class="row" style="margin-bottom:10px !important;">
        <ul class="pull-right">
            Sort:
            <li class="list-group-item sort" data-sort="my-order:desc">Recent</li>
            <li class="list-group-item sort" data-sort="my-order:asc">Older</li>
        </ul>
    </div>

    <h1 style="text-align: center;">Orders list for
    </h1>

    <h1 style="text-align: center;"> {{date('D, d M Y')}} </h1>


    <div class="table-responsive" style="margin-top:30px;">
        <table class="table table-striped ">
            <thead>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Time
                </th>
                <th>
                    Meal
                </th>
            </tr>
            </thead>
            <tbody id="Container">
            @foreach($orders as $order)
                <tr class='mix' data-my-order="{{$order->time->time}}">
                    <td>{{$order->user->first_name}} {{$order->user->last_name}}</td>
                    <td>{{$order->time->time}}</td>
                    <td>
                        {{$order->item->name}}
                        <small>
                            ({{$order->item->description}})
                        </small>
                    </td>
                </tr>
            @endforeach
            </tbody>


        </table>
    </div>
    <script>
        $(document).ready(function(){
            $('#Container').mixItUp({
                load: {
                    sort: 'my-order:desc'
                },
                controls: {
                    toggleFilterButtons: true,
                    toggleLogic: 'and'
                },
                layout: {
                    display: 'table-row'
                }
            });
        })
    </script>

@endsection
