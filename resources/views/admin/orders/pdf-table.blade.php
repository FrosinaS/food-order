<style>
    table, th, td {
        border: 1px solid black;
    }
    td{

        text-align:center;
    }
    table {
        border-collapse: collapse;
    }
</style>
<h1 style="text-align: center;">Orders table for
</h1>
<?php     $nextweek = strtotime('next week');
        $dates = array(
                date('Y-m-d', strtotime('monday', $nextweek)),
                date('Y-m-d', strtotime('tuesday', $nextweek)),
                date('Y-m-d', strtotime('wednesday', $nextweek)),
                date('Y-m-d', strtotime('thursday', $nextweek)),
                date('Y-m-d', strtotime('friday', $nextweek))
        );
        ?>
<h1 style="text-align: center;"><b>{{date('D, d M ', strtotime($dates[0]))}}</b> -
    <b>{{date('D, d M ', strtotime($dates[4]))}}</b></h1>


<div class="table-responsive" style="margin-top:30px;">
    <table class="table table-bordered ">
        <thead>
        <tr>
            <th>Menu Items</th>

            @foreach($dates as $date)

                <th colspan="{{$numTimes}}">
                    {{date('D, d M', strtotime($date))}}
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <?php
            for($i = 0; $i < 5; $i++){
            foreach($times as $time){
            ?>
            <td>{{date('H:i',strtotime($time->time))}}</td>
            <?php  }
            }
            ?>
        </tr>
        @foreach($orders as $order => $date)
            <tr>
                <td>
                    {{$order}}
                </td>
                @foreach($dates as $datum)
                    @foreach($times as $time)
                        <td>
                            @if(array_key_exists($datum, $date) && array_key_exists($time->time, $date[$datum]))
                                {{$date[$datum][$time->time]}}
                            @endif
                        </td>
                    @endforeach
                @endforeach
            </tr>
        @endforeach
        </tbody>

    </table>
</div>
