@extends('admin.admin-main-layout')
@section('main-content')
    <style>
        table tr th {
            text-align: center;
        }
    </style>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>Orders table</li>
    </ul>

    @include('admin.orders.pdf-table')

    <button class="btn btn-primary pull-right download">Download PDF</button>
    <script>
        $(document).ready(function(){
            $('.download').click(function(){
                window.location.replace('/admin/pdf-table');
            })
        })
    </script>
    @endsection




