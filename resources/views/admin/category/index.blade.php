@extends('admin.admin-main-layout')

@section('main-content')

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <a class="btn btn-default navbar-btn" href="categories/create">New</a>
        <a class="btn btn-default navbar-btn" id='activate_btn'>Activate</a>
        <a class="btn btn-default navbar-btn" id='deactivate_btn'>Deactivate</a>
        <button class="delete_btn btn btn-default navbar-btn" id="delete_btn" >Delete</button>
        <form class="navbar-form navbar-right" role="search">
<!--            <div class="form-group">
                <input type="text" class="form-control" id="search_query" placeholder="Search">
            </div>
            <button type="submit" id="search" class="btn btn-default">Submit</button>-->
            
            
            
            <input type="text" class="search-query" placeholder="Search" id="search_query" value="<?php echo Input::get('search'); ?>">
        <button class="btn" id="search" style="margin-left: 10px;" ><i class="icon-search"></i> Search</button>
        </form>
<!--        <button type="button" class="btn btn-default navbar-btn">Reset</button>-->

    </div><!-- /.container-fluid -->
</nav>


<form id="read_category_form" action="" data-url='{{\URL::to('/admin/categories')}}' method="post">
    {!! Form::token() !!}

    <table  class="table table-striped">
        <thead>
            <tr style="font-weight: bold;" style="padding: 10px;">

                <th><input type="checkbox" id="select_all" name="select_all" value="0"></th>
                <th>Name</th>
                <th>Description</th>
                <th>Status</th>
                <th>Created at</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($categories)) {
                foreach ($categories as $category):
                    ?>

                    <tr>
                        <th><input type="checkbox" name="{!! $category->id; !!}" value="{!! $category->id; !!}" class="check"></th>
                        <td>{!! $category->category_name; !!}</td>
                        <td>{!! $category->description; !!}</td>
                        <td><?php echo ($category->category_status == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-important">Not active</span>'; ?></td>
                        <td>{!! $category->created_at; !!}</td>
                        <td><a href="categories/{!! $category->id;!!}/edit">Edit</a></td>   
                    </tr>

    <?php endforeach;
} ?>

        </tbody>
    </table> 

</form>
<div class="page-header">
    Filter items by:
</div>
<ul class="nav nav-list fiter_users">

    <li class="nav-header">Date created</li>
    <li <?php if (Input::get('created') == 'recent') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="created" data-created="recent">Recent</a>
        <?php if (Input::get('created') == 'recent') { ?> 
            <a href="javascript://" class="data_remove remove_created" title="Remove filter"><i class="icon-remove"></i></a>
<?php } ?>
    </li>
    <li <?php if (Input::get('created') == 'old') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="created" data-created="old">Older</a>
        <?php if (Input::get('created') == 'old') { ?> 
            <a href="javascript://" class="data_remove remove_created" title="Remove filter"><i class="icon-remove"></i></a>
<?php } ?>
    </li>

    <li class="nav-header">Status</li>
    <li <?php if (Input::get('status') == 'active') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="status" data-status="active">Active</a>
        <?php if (Input::get('status') == 'active') { ?> 
            <a href="javascript://" class="data_remove remove_status" title="Remove filter"><i class="icon-remove"></i></a>
<?php } ?>
    </li>
    <li <?php if (Input::get('status') == 'not_active') { ?> class="active" <?php } ?>>
        <a href="javascript://" class="status" data-status="not_active">Not active</a>
        <?php if (Input::get('status') == 'not_active') { ?> 
            <a href="javascript://" class="data_remove remove_status" title="Remove filter"><i class="icon-remove"></i></a>
<?php } ?>
    </li>
</ul>

<input type="hidden" id="category" value="<?php echo Input::get('category') ?>" />
<input type="hidden" id="price" value="<?php echo Input::get('price') ?>" />
<input type="hidden" id="created" value="<?php echo Input::get('created') ?>" />
<input type="hidden" id="status" value="<?php echo Input::get('status') ?>" />
@endsection


<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
<script type="text/javascript">

$(document).ready(function () {

    $("#select_all").on('click', function () {

        if (this.checked) {
            // Iterate each checkbox
            $('.check').each(function () {
                this.checked = true;

            });
        }
        else {
            $('.check').each(function () {
                this.checked = false;
            });
        }


    });

    $('#delete_btn').on('click', function () {
        var url = $("#read_category_form").attr('data-url') + '/deleteCategory';

        $("#read_category_form").attr('action', url);
        $("#read_category_form").submit();
    });

    $('#activate_btn').on('click', function () {
        var url = $("#read_category_form").attr('data-url') + '/activateCategory';

        $("#read_category_form").attr('action', url);
        $("#read_category_form").submit();
    });

    $('#deactivate_btn').on('click', function () {
        var url = $("#read_category_form").attr('data-url') + '/deactivateCategory';

        $("#read_category_form").attr('action', url);
        $("#read_category_form").submit();
    });


    function search_query() {


        var status = $("#status").val();
        var created = $("#created").val();
        var search = $("#search_query").val();
        // var search = $("#search_query").val();
        //alert(search);
        var query = [];

        if (category.length > 0) {
            query.push('category=' + category);
        }
        if (status.length > 0) {
            query.push('category_status=' + status);

        }
        if (price.length > 0) {
            query.push('price=' + price);
        }
        if (created.length > 0) {
            query.push('created=' + created);
        }
        if (search.length > 0) {
            query.push('search=' + search);
        }

        var query_url = 'admin/categories?' + query.join('&');
        var url = window.location.protocol + "//" + window.location.host + "/" + query_url;
        alert("URL:" +url);
        location.href = url;

    }



    $('.fiter_users').find('a.created').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('#created').val($(this).attr('data-created'));
        search_query();
    });

    $('.fiter_users').find('a.remove_created').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('#created').val('');
        search_query();
    });

    $('.fiter_users').find('a.status').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('#status').val($(this).attr('data-status'));
        search_query();
    });

    $('.fiter_users').find('a.remove_status').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('#status').val('');
        search_query();
    });
   $("#search").on('click', function() {
        search_query();
    });
    
    
    $("#search_query").on('keypress', function(e) {
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if(keycode == '13'){
			search_query();
		}
    });
})


</script>

</head>

