@extends('admin.admin-main-layout')

@section('main-content')
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <a class="btn btn-default navbar-btn" href="{{\URL::to('/admin/items')}}">Back</a>
    </div><!-- /.container-fluid -->
</nav>


{!! Form::open(array('url'=>'/admin/categories', 'method' => 'post', 'id' => 'categories-create-form')) !!}

<!--   {!! Form::token() !!}-->
<div class="form-group">
    {!! Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')); !!}
    {!!  Form::text('name', '',  array('class' => 'form-control')); !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')); !!}
    {!!  Form::text('description', '', array('class' => 'form-control')); !!}
</div>

<div class="form-group">
    {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
    {!! Form::radio('status', '1');!!} Active
    {!! Form::radio('status', '0');!!} Not Active
</div>


<button type="submit" class="btn btn-default">Submit</button>
</div>
<div class="form-group">

</div>
{!! Form::close() !!}

@endsection
