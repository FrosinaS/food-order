@extends('admin.admin-main-layout')

@section('main-content')
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <a class="btn btn-default navbar-btn" href="{{\URL::to('/admin/items')}}">Back</a>
    </div><!-- /.container-fluid -->
</nav>


{!! Form::open(array('url'=>'/admin/categories/'.$category->id, 'method' => 'put', 'id' => 'category-edit-form')) !!}

<!--   {!! Form::token() !!}-->
<div class="form-group">
    {!! Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')) !!}
    {!! Form::text('name', $category->category_name,  array('class' => 'form-control')) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) !!}
    {!! Form::text('description', $category->description, array('class' => 'form-control')) !!}
</div>





<?php
if ($category->status == '1') {
    $active = true;
    $not_active = false;
} else {
    $active = false;
    $not_active = true;
}
?>
<div class="form-group">
    {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
    {!! Form::radio('status', '1', $active);!!} Active
    {!! Form::radio('status', '0', $not_active);!!} Not Active
</div>

<div class="col-sm-6">
    <button type="submit" class="btn btn-default">Submit</button>
</div>
</div>
<div class="form-group">

</div>
{!! Form::close() !!}

@endsection
