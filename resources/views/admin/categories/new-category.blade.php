@extends('admin.admin-main-layout')
@section('main-content')
    <div class="container-fluid" style="text-align:center;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="background-color:#1abc9c;">
                    <h3>Food Order </h3>

                    <div class="panel-heading h4"
                         style="background-color:#1abc9c; border-top:2px solid slategray; border-bottom: none;">
                        {!! $header !!}

                    </div>
                    <div class="panel-body" style="background-color:white; border-top:2px solid slategray;">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <form class="form-horizontal" role="form" method="POST" style="margin-top:30px;">


                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <div class="form-group row">
                                <label class="col-md-4 control-label">Name: </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name"
                                           value="{{$category->name}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label">Description: </label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="description"
                                           value="{{$category->description}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 control-label">Status: </label>

                                <div class="col-md-6">
                                    <select name="status" class="form-control">
                                        <option value="active" >Active</option>
                                        <option value="not-active">Not active</option>
                                    </select>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('select[name^="status"] option:selected').attr("selected", null);
                                        $('select[name^="status"] option[value="{{$category->status == 1 ? 'active' : 'not-active'}}"]').attr("selected", "selected");

                                    })
                                </script>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary pull-right">
                                        {!! $btnText !!}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>



@endsection
