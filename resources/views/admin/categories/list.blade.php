@extends('admin.admin-main-layout')
@section('main-content')
    <style>
        .status {
            width: 120px;
        }

        table tr th {
            text-align: center;
        }

        table tr td {
            width: auto;
            padding: 15px !important;
            text-align: center !important;
        }
        .label-success
        {
            background-color:#1abc9c !important;
        }
        .checkbox
        {
            margin-left:50px !important;
        }

        table th:first-child {
            width: 181px !important;
        }
        .list-group-item {
            display: inline-block !important;
            width: auto;
            margin-right: 10px;
            margin-bottom:10px;
        }

        #Container .mix
        {
            display:none;
        }

        .pager-list  .active
        {
            background-color:lightgray;
            color:gray;
            font-weight:bold;
        }
        .well div:first-child > button
        {
            margin-right: 3px;
            margin-top:10px;
        }

        .pager
        {
            background-color:#1abc9c !important;
        }



    </style>

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a src="">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>Manage categories</li>
    </ul>


    <div class="row" style="margin-bottom:10px !important;">
        <ul class="pull-right" >
            Sort:
            <li class="list-group-item sort" data-sort="my-order:desc">Recent</li>
            <li class="list-group-item sort" data-sort="my-order:asc">Older</li>
            Filter:
            <li class="list-group-item filter" data-filter="all">All</li>
            <li class="list-group-item filter activeUser" data-filter=".active">Active</li>
            <li class="list-group-item filter notActiveUser" data-filter=".not-active">Not active</li>
        </ul>
    </div>
    <div class="well">
        <div class="row">
            <div class="col-md-8">
                <button class="btn btn-default addNewCategory"><span class="glyphicon glyphicon-plus"></span> New
                </button>
                <button class="btn btn-primary activateCategories"><span class="glyphicon glyphicon-ok"></span> Activate
                </button>
                <button class="btn btn-warning deactivateCategories"><span class="glyphicon glyphicon-remove"></span>
                    Deactivate
                </button>
                <button class="btn btn-danger deleteCategories"><span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" id="search-query-3">
                          <span class="input-group-btn">
                            <button type="submit" class="btn search"><span class="fui-search"></span></button>
                          </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table  table-striped ">
            <thead>
            <tr>
                <th>
                    <button type="button" class="btn btn-link selectAll"><span class="glyphicon glyphicon-ok"></span>
                        <span class="text">Select all</span>
                    </button>
                </th>
                <th>
                    ID
                </th>
                <th>
                    Name
                </th>
                <th>
                   Description
                </th>
                <th>
                    Status
                </th>
                <th>
                    Created at
                </th>
                <th>
                    Actions
                </th>
            </tr>
            </thead>
            <tbody id="Container">
            @foreach($categories as $category)
                <tr class="mix {{$category->status == 1 ? 'active' : 'not-active'}}" data-my-order="{{$category->id}}">
                    <td>
                        <label class="checkbox" >
                            <input type="checkbox" class="checkCategory" data-toggle="checkbox"  id="{{$category->id}}">
                        </label>
                    </td>
                    <td>
                        {{$category->id}}
                    </td>
                    <td>
                        {{$category->name}}
                    </td>
                    <td>
                        {{$category->description}}
                    </td>
                    <td class="status">
                        @if($category->status == 1)
                            <div class="label label-success">
                                Active
                            </div>
                        @else
                            <div class="label label-danger ">
                                Not active
                            </div>
                        @endif
                    </td>
                    <td>
                        {{date('D, d M Y, H:i', strtotime($category->created_at))}}
                    </td>
                    <td>
                        <a href="/admin/categories/edit-category/{{$category->id}}">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>


        </table>

        <div class="pager-list pull-right">

        </div>
    </div>

    <script>
        $(document).ready(function () {

            $("input[type='checkbox']").radiocheck();


            $(".addNewCategory").click(function () {
                window.location.replace("/admin/categories/add-new");
            });

            $(".activateCategories").click(function () {
                var categories = $(":checkbox:checked");
                if(categories.length > 0) {
                    var categoryIds = [];
                    categories.each(function () {
                        categoryIds.push($(this).attr('id'));
                    });
                    $.ajax({
                        url: '/admin/categories/activate',
                        method: 'POST',
                        data: {
                            categoryIds: categoryIds,
                            _token: '{{ csrf_token()}}'
                        },
                        success: function () {
                            categories.each(function () {
                                $(this).parent().parent().parent().find('.status').html("<div class='label label-success'>Active</div>");
                                $(this).parent().parent().parent().removeClass('not-active').addClass('active');
                                $(this).prop('checked', false);
                                $(".text").html("Select all");
                            })
                        }
                    });
                }
            });

            $(".deactivateCategories").click(function () {
                var categories = $(":checkbox:checked");
                if(categories.length > 0) {
                    var categoriesIds = [];
                    categories.each(function () {
                        categoriesIds.push($(this).attr('id'));
                    });
                    $.ajax({
                        url: '/admin/categories/deactivate',
                        method: 'POST',
                        data: {
                            categoryIds: categoriesIds,
                            _token: '{{ csrf_token()}}'
                        },
                        success: function () {
                            categories.each(function () {
                                $(this).parent().parent().parent().find('.status').html("<div class='label label-danger'>Not active</div>");
                                $(this).parent().parent().parent().removeClass('active').addClass('not-active');
                                $(this).prop('checked', false);
                                $(".text").html("Select all");
                            })
                        }
                    });
                }
            });

            $(".deleteCategories").click(function () {
                if($(":checkbox:checked").length > 0) {
                    $(".modal-title").html("Delete users");
                    $(".modal-body").html("Are you sure you want to delete these users?");
                    $("#basicModal").modal();
                }
            });

            $("#no").click(function () {
                $("#basicModal").modal('hide');
            });

            $("#yes").click(function () {
                var categories = $(":checkbox:checked");
                var categoriesIds = [];
                categories.each(function () {
                    categoriesIds.push($(this).attr('id'));
                });
                $.ajax({
                    url: '/admin/categories/delete',
                    method: 'POST',
                    data: {
                        categoryIds: categoriesIds,
                        _token: '{{ csrf_token()}}'
                    },
                    success: function () {
                        $("#basicModal").modal('hide');
                        categories.each(function () {
                            $(this).parent().parent().parent().remove()
                        });

                    }
                });
            });

            $(".selectAll").click(function () {
                if ($(".text").html() == "Select all") {
                    $(".checkCategory").prop("checked", true);
                    $(".text").html("Unselect all");
                }
                else {
                    $(".checkCategory").prop("checked", false);
                    $(".text").html("Select all");
                }
            });

            $(".search").click(function () {
                window.location.replace('/admin/categories/search/' + $("#search-query-3").val());
            });

            $('#Container').mixItUp({
                load: {
                    filter: 'all',
                    sort: 'my-order:desc'
                },
                controls: {
                    toggleFilterButtons: true,
                    toggleLogic: 'and'
                },
                layout: {
                    display: 'table-row'
                },
                pagination: {
                    limit:5,
                    generatePagers:true,
                    pagerClass: 'btn',
                    prevButtonHTML: 'Previous',
                    nextButtonHTML: 'Next',
                    loop: false,
                    load: 1
                }
            });

        });
    </script>

    @include("admin.partials.modal")
@endsection


