@extends('main-layout')

@section('content')

@include('admin.partials.admin-navbar')


<!-- start: Main Container -->




<link id="base-style" href="/layouts/Bootstrap_Metro_DashBoard/css/style.css" rel="stylesheet">
<link id="base-style-responsive" href="/layouts/Bootstrap_Metro_DashBoard/css/style-responsive.css"
      rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext'
      rel='stylesheet' type='text/css'>
<!-- end: CSS -->

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<link id="ie-style" href="/layouts/Bootstrap_Metro_DashBoard/css/ie.css" rel="stylesheet">
<![endif]-->

<!--[if IE 9]>
<link id="ie9style" href="/layouts/Bootstrap_Metro_DashBoard/css/ie9.css" rel="stylesheet">
<![endif]-->
<!-- start: JavaScript-->


<div class="container-fluid-full" style="height:auto !important; display: block;">
    <div class="row-fluid">

        <!-- include Side bar-->

        @include('admin.partials.admin-sidebar')


        <!-- start: Content -->
        <div id="content" class="span10" style="height:auto; min-height:1000px;">


               @yield('main-content')

        </div>


    </div>
</div>
<!--<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>-->
@stop