<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        
        
        $user =
            [
                "role"          => 1,
                "name"          => "Admin",
                "first_name"    => "Sanja",
                "last_name"     => "Blazevska",
                "email"         => 'foodadmin@cosmicdevelopment.com',
                "password"      => 'admin123',
                "confirmation"  => 1,
                "vegetarian"    => 1,
                "status"        => 1,
            ];
        
        
        
        $user = User::create($user);
        if($user)   
            $this->command->info("Admin seeded!");
        else
            $this->command->info("Admin seed fail!");
    } 
}